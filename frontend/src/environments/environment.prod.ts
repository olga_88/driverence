export const environment = {
  production: true,
  baseHref: '/driverence',
  backendUrl: '/api',
  wsUrl: 'https://panel.whencyber.com/',
  signalServer: 'wss://panel.whencyber.com/broadcast',
  userLoginUrl: 'https://panel.whencyber.com/central/auth/login'
};
