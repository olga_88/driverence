import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from '../shared/error404/error404.component';

import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent
  },
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DriverRoutingModule { }