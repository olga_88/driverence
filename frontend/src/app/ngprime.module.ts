import { NgModule } from '@angular/core';

import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ButtonModule} from 'primeng/button';
import {GMapModule} from 'primeng/gmap';
import {TooltipModule} from 'primeng/tooltip';
import {MessageModule} from 'primeng/message';
import {CardModule} from 'primeng/card';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {MenubarModule} from 'primeng/menubar';
import {TableModule} from 'primeng/table';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {ListboxModule} from 'primeng/listbox';
import {AvatarModule} from 'primeng/avatar';
import {AvatarGroupModule} from 'primeng/avatargroup';
import {ToolbarModule} from 'primeng/toolbar';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';

import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import {ToastModule} from 'primeng/toast';
import {SelectButtonModule} from 'primeng/selectbutton';

import {SidebarModule} from 'primeng/sidebar';
import {PickListModule} from 'primeng/picklist';
import {InputSwitchModule} from 'primeng/inputswitch';
import {DropdownModule} from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';


// other UI third parties
import { NgProgressModule } from 'ngx-progressbar';

@NgModule({
	imports: [
		NgProgressModule.withConfig({
			thick: true,
			spinner: false
		})
	],

	exports: [
		InputTextModule,
		ButtonModule,
		GMapModule,
		TooltipModule,
		MessageModule,
		CardModule,
		OverlayPanelModule,
		TableModule,
		BreadcrumbModule,
		ListboxModule,
		AvatarModule,
		AvatarGroupModule,
		MenubarModule,
		ToolbarModule,
		DialogModule,
		ConfirmDialogModule,
		InputTextareaModule,
		ToastModule,
		SelectButtonModule,
		DynamicDialogModule,
		SidebarModule,
		PickListModule,
		InputSwitchModule,
		DropdownModule,
		AutoCompleteModule,

		NgProgressModule
	],

	providers: [
		ConfirmationService,
		MessageService,
		DialogService
	]
})
export class NgPrimeModule { }