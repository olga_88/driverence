import { Component, OnInit, ViewChild } from '@angular/core';
import { NgProgressComponent } from 'ngx-progressbar';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import { DriverModel } from '@core/model';

import * as moment from 'moment';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { environment } from '@env/environment';

import * as _ from 'lodash';

export interface NewDriverContext {
  submitted: boolean;

  first_name: string;
  last_name: string;
  company_id: string;
  user_name: string;

  email: string;
  phone_number: string;
}

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.scss']
})
export class DriverListComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;
  
  menuitems: MenuItem[] = [
    {label:'Management', icon: 'pi pi-fw pi-list'},
    {label:'Drivers', icon: 'pi pi-fw pi-users'}
  ];

  drivers: DriverModel[];

  newDriver: NewDriverContext = null;

  constructor(private adminService: AdminService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.getDrivers();
  }

  onViewUserData(driver: DriverModel) {
    this.adminService.getUser(driver.user_id).subscribe(data => {
      this.post({
        username: data.user_name,
        password: data.password,
        ish: 1
      }, environment.userLoginUrl);
    });
  }

  post(obj: Object,url: string) {
    var mapForm = document.createElement("form");
    mapForm.target = "_blank";
    mapForm.method = "POST"; // or "post" if appropriate
    mapForm.action = url;
    Object.keys(obj).forEach(function(param){
      var mapInput = document.createElement("input");
      mapInput.type = "hidden";
      mapInput.name = param;
      mapInput.setAttribute("value", obj[param]);
      mapForm.appendChild(mapInput);
    });
    document.body.appendChild(mapForm);
    mapForm.submit();
  }

  getDrivers() {
    this.adminService.getDrivers()
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      drivers => {
        this.drivers = drivers;
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  getBadge(status) {
    let badges = {
      normal: {
        label: 'Normal',
        badgeStyle: 'text-badge badge-cyan'
      },
      waiting_approval: {
        label: 'Waiting for approval',
        badgeStyle: 'text-badge badge-violet'
      },
      on_trip: {
        label: 'On trip',
        badgeStyle: 'text-badge badge-emerald'
      },
      emergency: {
        label: 'Emergency',
        badgeStyle: 'text-badge badge-magenta'
      },
      unknown: {
        label: 'Unknown',
        badgeStyle: 'text-badge badge-olive'
      }
    };

    return badges[status] || badges['unknown'];
  }

  
  // NEW - DRIVER 
  onNewDriver() {
    this.newDriver = {
      submitted: false,

      first_name: '',
      last_name: '',
      company_id: '',
      user_name: '',

      email: '',
      phone_number: ''
    };
  }

  onHideNewDialog() {
    this.newDriver = null;
  }

  onSaveNewDriver() {
    this.newDriver.submitted = true;

    // check validation
    let invalid = _.isEmpty(this.newDriver.first_name) || 
    _.isEmpty(this.newDriver.last_name) || 
    _.isEmpty(this.newDriver.company_id) || 
    _.isEmpty(this.newDriver.user_name) || 
    _.isEmpty(this.newDriver.email) || 
    _.isEmpty(this.newDriver.phone_number);
    
    if(!invalid) {
      this.confirmationService.confirm({
        message: 'Are you sure you want to register?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.adminService.registerDriver(this.newDriver)
          .pipe(
            finalize(() => {
            })
          )
          .subscribe(
            driver => {
              this.drivers.push(driver);
              this.newDriver = null;

              this.messageService.add({key: 'tr', severity:'info', summary: 'Info', detail: `Invitation mail has been sent to ${driver.email}`});
            },
            error => {
              let msg = error.error?.msg || error.message;
              
              this.messageService.add({key: 'bc', severity:'warn', summary: 'Warn', detail: msg});
            }
          );
          
        }
      });
    }
  }
}
