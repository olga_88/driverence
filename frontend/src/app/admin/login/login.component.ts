import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { NgProgressComponent } from 'ngx-progressbar';
import { Message } from 'primeng/api';
import { finalize } from 'rxjs/operators';

import { AdminService } from '../admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;

  constructor(private adminService: AdminService, 
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder) { 
      this.createForm();
    }

  ngOnInit(): void {
  }

  createForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get controls() {
    return this.form.controls;
  }

  login() {
    this.error = null;

    let {email, password} = this.form.value;
    this.adminService.login({
      email, password
    })
    .pipe(
      finalize(() => {
        this.form.markAsPristine();
        this.form.enable();
        this.progressBar.complete();
      })
    )
    .subscribe(
      body => {
        const { token, user } = body;
          if (token && user) {
            this.authenticationService.login(user, token, false);
            this.route.queryParams.subscribe(params =>
              this.router.navigate([params.redirect || '/admin'], {
                replaceUrl: true
              })
            );
          }
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
    

    this.form.disable();
    this.progressBar.start();

  }
}
