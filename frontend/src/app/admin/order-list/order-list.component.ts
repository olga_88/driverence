import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderModel } from '@app/core/model';
import { SocketService } from '@app/core/socket/socket.service';
import * as _ from 'lodash';
import { NgProgressComponent } from 'ngx-progressbar';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import { NewTripComponent } from '../new-trip/new-trip.component';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;

  menuitems: MenuItem[] = [
    {label:'Management', icon: 'pi pi-fw pi-list'},
    {label:'Trip Orders', icon: 'pi pi-fw pi-calendar'}
  ];

  orders: OrderModel[] = [];

  newTripRef: DynamicDialogRef;
  newTripOpen: boolean = false;

  constructor(
    private adminService: AdminService,
    private dialogService: DialogService,
    private socketService: SocketService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders() {
    this.adminService.getOrders()
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      orders => {
        this.orders = _.map(orders, order => {
          return _.merge(order, {
            driver: _.find(this.socketService.drivers, { driver_id: order.driver_id })
          });
        });
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  getBadge(status) {
    let badges = {
      pending: {
        label: 'Pending',
        badgeStyle: 'text-badge badge-cyan'
      },
      accepted: {
        label: 'Accepted',
        badgeStyle: 'text-badge badge-violet'
      },
      rejected: {
        label: 'Rejected',
        badgeStyle: 'text-badge badge-emerald'
      },
      canceled: {
        label: 'Canceled',
        badgeStyle: 'text-badge badge-magenta'
      },
      unknown: {
        label: 'Unknown',
        badgeStyle: 'text-badge badge-olive'
      }
    };

    return badges[status] || badges['unknown'];
  }

  onNewOrder() {
    this.newTripRef = this.dialogService.open(NewTripComponent, {
      header: `Create Trip Order`,
    });

    this.newTripRef.onClose.subscribe(() => {
      this.newTripOpen = false;
      this.getOrders();
    });

    this.newTripOpen = true;
  }

  onCancelOrder(order: OrderModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to cancel this order?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.adminService.cancelOrder(order.order_id)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          order => {
            this.getOrders();
          },
          error => {
            let msg = error.error?.msg || error.message;
            this.messageService.add({key: 'tr', severity:'warn', summary: 'Warn', detail: msg});
          }
        );
      }
    });
  }

  onDeleteOrder(order: OrderModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this order?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.adminService.deleteOrder(order.order_id)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          order => {
            this.getOrders();
          },
          error => {
            let msg = error.error?.msg || error.message;
            this.messageService.add({key: 'tr', severity:'warn', summary: 'Warn', detail: msg});
          }
        );
      }
    });
  }
}
