import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { CallModel, DriverModel } from '@core/model';
import { NgProgressComponent } from 'ngx-progressbar';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import * as moment from 'moment';
import * as _ from 'lodash';

interface NewDialogContext {
  message: string;
  answerRequired: boolean;
  file: File;
}

interface NewCallContext {
  summary: string;
  dialogs: NewDialogContext[];

  submitted: boolean;
}

interface EditCallContext {
  call_id: number;
  summary: string;
  drivers: {
    available: DriverModel[],
    selected: DriverModel[]
  },
  is_default: boolean;

  submitted: boolean;
}

@Component({
  selector: 'app-call-list',
  templateUrl: './call-list.component.html',
  styleUrls: ['./call-list.component.scss']
})
export class CallListComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;
  
  drivers: DriverModel[];
  calls: CallModel[];
  menuitems: MenuItem[] = [
    {label:'Management', icon: 'pi pi-fw pi-list'},
    {label:'Questions', icon: 'pi pi-fw pi-volume-up'}
  ];

  durations = [
    // { label: '2 minutes', value: moment.duration(2, 'm').asMilliseconds()},
    // { label: '10 minutes', value: moment.duration(10, 'm').asMilliseconds()},
    { label: '20 minutes', value: moment.duration(20, 'm').asMilliseconds()},
    { label: '30 minutes', value: moment.duration(30, 'm').asMilliseconds()},
    { label: '1 hour', value: moment.duration(1, 'h').asMilliseconds()},
    { label: '2 hours', value: moment.duration(2, 'h').asMilliseconds()},
    { label: '3 hours', value: moment.duration(3, 'h').asMilliseconds()},
    { label: '4 hours', value: moment.duration(4, 'h').asMilliseconds()},
  ];

  selectedDuration: number = this.durations[0].value;

  newCall: NewCallContext = null;

  editCall: EditCallContext = null;
  
  constructor(private adminService: AdminService,
    private santizier: DomSanitizer,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.getCalls();

    this.getCallDuration();
  }

  getCallDuration() {
    this.adminService.getCallDuration().subscribe(duration => {
      console.log('call duration', duration);
      this.selectedDuration = duration;
    });
  }
  
  getCalls() {
    this.adminService.getCalls()
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      calls => {
        this.calls = calls;

        this.adminService.getDrivers()
        .subscribe(drivers => {
          this.drivers = drivers;
          _.forEach(calls, call => {
            this.postProcessCall(call);
          });
        });
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  postProcessCall(call: CallModel) {
    // Show drivers avatar limit 10
    let drivers = _.map(call.driver_ids, (driver_id) =>  _.find(this.drivers, {driver_id}));
    call.drivers = _.take(drivers, 10);

    _.forEach(call.dialogs, dialog => {
      dialog.safe_url = this.getAudioUrl(dialog.file_path);
    });
  }

  getAudioUrl(path) {
    let token = this.authService.credentials.token;
    path = `/api/assets/${path}?token=${token}`
    let url = this.santizier.bypassSecurityTrustUrl(path);
    return url;
  }


  onDeleteCall(index) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this call?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.adminService.deleteCall(this.calls[index].call_id).subscribe(
            res => {
              // this.calls.splice(index, 1);

              this.getCalls();
            },
            error => {
              let msg = error.error?.msg || error.message;
              this.error = msg;
            });
      }
    });
  }

  // EDIT - CALL
  // trackBy(index: number, item: DriverModel) {
  //   return item.driver_id;
  // }

  onEditCall(call: CallModel) {
    
    this.editCall = {
      call_id: call.call_id,
      summary: call.summary,
      drivers: {
        selected: call.driver_ids.map(driver_id => _.find(this.drivers, {driver_id})),
        available: this.drivers.filter(d => call.driver_ids.indexOf(d.driver_id) < 0)
      },
      is_default: call.is_default,

      submitted: false
    };
  }

  onHideEditDialog() {
    this.editCall = null;

  }

  onSaveEditCall() {
    let driver_ids = this.editCall.drivers.selected.map(d => d.driver_id);
    let is_default = this.editCall.is_default;

    this.confirmationService.confirm({
      message: 'Are you sure you want to save this call?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.adminService.updateCall(this.editCall.call_id, {
          driver_ids, is_default
        })
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(call => {
          this.editCall = null;
          this.getCalls();
        });
      }
    });
  }

  // NEW - CALL 
  onNewCall() {
    this.newCall = {
      summary: '',
      dialogs: [],
      submitted: false
    };
    this.onNewDialog();
  }

  onHideNewDialog() {
    this.newCall = null;
  }

  onSaveNewCall() {
    
    this.newCall.submitted = true;

    // check validation
    let invalid = this.newCall.summary.trim().length === 0 || this.newCall.dialogs.length === 0;
    _.forEach(this.newCall.dialogs, (dialog, index) => {
      if(!dialog.message.trim()) invalid = true;
      if(!dialog.file) invalid = true;

      if(index < this.newCall.dialogs.length - 1) {
        dialog.answerRequired = true;
      }
    });

    if(!invalid) {
      this.confirmationService.confirm({
        message: 'Are you sure you want to save this call?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.adminService.uploadCall(this.newCall)
          .pipe(
            finalize(() => {
            })
          )
          .subscribe(call => {
            this.postProcessCall(call);
            this.calls.push(call);
            this.newCall = null;
          });
        }
      });
    }
  }

  onNewDialog() {
    this.newCall.dialogs.push({
      message: "",
      answerRequired: true,
      file: null
    });
  }

  onDeleteDialog(index) {
    this.newCall.dialogs.splice(index, 1);
  }

  onSelectedDurationChanged(event) {
    this.adminService.setCallDuration(this.selectedDuration).subscribe(duration => {
      console.log('new duration', duration);
      this.selectedDuration = duration;
    });
  }

  handleFileInput(files: FileList, index: number) {
    this.newCall.dialogs[index].file = files.item(0);
  }

  // UI Utils
  firstLetter(driver: DriverModel) {
    if(!driver) {
      return 'X'
    };
    return driver.first_name[0].toUpperCase() + driver.last_name[0].toUpperCase();
  }
}
