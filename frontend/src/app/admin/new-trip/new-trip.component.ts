import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DriverModel } from '@app/core/model';
import { SocketService } from '@app/core/socket/socket.service';
import { AdminService } from '../admin.service';

import * as _ from 'lodash';
import { ConfirmationService, MessageService } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

export interface TripOrderContext {
  driver_id: number;

  destination: string;
  latitude: number;
  longitude: number;
}

@Component({
  selector: 'app-new-trip',
  templateUrl: './new-trip.component.html',
  styleUrls: ['./new-trip.component.scss']
})
export class NewTripComponent implements OnInit {
  @ViewChild("inputPlace") inputPlace: ElementRef;

  drivers: DriverModel[] = [];
  map: google.maps.Map;
  
  marker: google.maps.Marker;
  infoWindow: google.maps.InfoWindow;

  selectedDriver: DriverModel = null;
  selectedPlace: google.maps.places.PlaceResult = null;

  constructor(
    private socketService: SocketService,
    private adminService: AdminService,
    private dialogRef: DynamicDialogRef,
    private confirmationService: ConfirmationService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.marker = new google.maps.Marker();
    this.marker.setVisible(false);

    this.infoWindow = new google.maps.InfoWindow();
    this.infoWindow.close();
  }

  get mapOptions() {
    return {
      center: {lat: 36.890257, lng: 30.707417},
      zoom: 12
    };
  }
  
  searchDriver(event) {
    let query = _.toLower(event.query);
    // this.drivers = _.filter(this.socketService.drivers, {status: 'normal'}).filter(
    this.drivers = _.filter(this.socketService.drivers, 
      driver => {
        return _.toLower(driver.full_name).indexOf(query) >= 0 ||
        _.toLower(driver.email).indexOf(query) >= 0 ||
        _.toLower(driver.phone_number).indexOf(query) >= 0
      }
    );
  }

  onMapReady(event) {
    this.map = event.map;

    const options = {
      fields: ["formatted_address", "geometry", "name"],
      origin: this.map.getCenter(),
      strictBounds: false
    };

    const autocomplete = new google.maps.places.Autocomplete(this.inputPlace.nativeElement, options);
    autocomplete.set('z-index', 1051);
    autocomplete.addListener("place_changed", () => {
      this.infoWindow.close();
      this.marker.setVisible(false);

      const place = autocomplete.getPlace();
  
      if (!place.geometry || !place.geometry.location) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        console.error("No details available for input: '" + place.name + "'");
        this.selectedPlace = null;
        return;
      }
  
      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        this.map.fitBounds(place.geometry.viewport);
      } else {
        this.map.setCenter(place.geometry.location);
        this.map.setZoom(17);
      }
      this.marker.setPosition(place.geometry.location);
      this.marker.setVisible(true);
      this.infoWindow.setContent(place.formatted_address);
      this.infoWindow.open(this.map, this.marker);

      this.selectedPlace = place;
    });
  }

  onSubmitOrder() {
    if(this.selectedDriver == null) {
      this.messageService.add({key: 'bc', severity:'warn', summary: 'Warn', detail: "Please select a driver"});
      return;
    }

    if(this.selectedPlace == null) {
      this.messageService.add({key: 'bc', severity:'warn', summary: 'Warn', detail: "Please pick a destination"});
      return;
    }

    let context: TripOrderContext = {
      driver_id: this.selectedDriver.driver_id,
      latitude: this.selectedPlace.geometry.location.lat(),
      longitude: this.selectedPlace.geometry.location.lng(),
      destination: this.selectedPlace.formatted_address
    };

    this.confirmationService.confirm({
      message: 'Are you sure you want to post this order?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.adminService.createOrder(context)
        .pipe(
          finalize(() => {
          })
        )
        .subscribe(
          order => {
            this.dialogRef.close();
          },
          error => {
            let msg = error.error?.msg || error.message;
            this.messageService.add({key: 'bc', severity:'warn', summary: 'Warn', detail: msg});
          }
        );
      }
    });
  }
}
