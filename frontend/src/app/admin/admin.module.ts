import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '../core/core.module';
import { LoginComponent } from './login/login.component';

import { AdminRoutingModule } from './admin-routing.module';
import { CallListComponent } from './call-list/call-list.component';
import { AdminService } from './admin.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgPrimeModule } from '@app/ngprime.module';
import { MainComponent } from './main/main.component';
import { MapComponent } from './map/map.component';
import { DriverListComponent } from './driver-list/driver-list.component';
import { TripListComponent } from './trip-list/trip-list.component';
import { MessagesComponent } from './messages/messages.component';
import { AlertsComponent } from './alerts/alerts.component';

import { MomentModule } from 'ngx-moment';
import { LiveComponent } from './live/live.component';
import { DataPrefetchGuard } from './data-prefetch.guard';
import { LoadingComponent } from './loading/loading.component';
import { VideoComponent } from './video/video.component';
import { RecordListComponent } from './record-list/record-list.component';
import { AppListComponent } from './app-list/app-list.component';
import { NewTripComponent } from './new-trip/new-trip.component';
import { OrderListComponent } from './order-list/order-list.component';

@NgModule({
  declarations: [LoginComponent, CallListComponent, MainComponent, MapComponent, DriverListComponent, TripListComponent, MessagesComponent, AlertsComponent, LiveComponent, LoadingComponent, VideoComponent, RecordListComponent, AppListComponent, NewTripComponent, OrderListComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgPrimeModule,
    MomentModule,

    CoreModule
  ],
  providers: [
    AdminService,
    DataPrefetchGuard
  ]
})
export class AdminModule { }
