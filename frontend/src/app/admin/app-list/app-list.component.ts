import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { AppModel } from '@app/core/model';
import { SocketService } from '@app/core/socket/socket.service';
import { NgProgressComponent } from 'ngx-progressbar';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';

import * as _ from 'lodash';

export interface NewAppContext {
  submitted: boolean;
  summary: string;
  version: string;
  file: File;
}

@Component({
  selector: 'app-app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['./app-list.component.scss']
})
export class AppListComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;
  
  newApp: NewAppContext = null;
  apps: AppModel[];

  menuitems: MenuItem[] = [
    {label:'Management', icon: 'pi pi-fw pi-list'},
    {label:'Apps', icon: 'pi pi-cloud-upload'}
  ];

  constructor(
    private socketService: SocketService,
    private adminService: AdminService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService) { }

  ngOnInit(): void {
    this.getApps();
  }

  
  getApps() {
    this.adminService.getApps()
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      apps => {
        this.apps = apps;

        console.log(apps);
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  getAssetUrl(path) {
    let token = this.authService.credentials.token;
    path = `/api/assets/${path}?token=${token}`
    return path;
  }

  onDownloadApp(app: AppModel) {
    saveAs(this.getAssetUrl(app.file_path), "campavias.apk");
  }

  onDeleteApp(app: AppModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this app?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let app_id = app.app_id;
        this.adminService.deleteApp(app_id).subscribe(
          res => {
            _.remove(this.apps, { app_id });
          },
          error => {
            let msg = error.error?.msg || error.message;
            this.error = msg;
          });
      }
    });
  }

  // NEW - APP 
  onNewApp() {
    this.newApp = {
      submitted: false,
      summary: '',
      version: '',
      file: null
    };
  }

  onHideNewDialog() {
    this.newApp = null;
  }

  handleFileInput(files: FileList) {
    this.newApp.file = files.item(0);
  }

  onSaveNewApp() {
    
    this.newApp.submitted = true;

    // check validation
    let invalid = _.isEmpty(this.newApp.summary) || _.isEmpty(this.newApp.version) || this.newApp.file === null;
    
    if(!invalid) {
      this.confirmationService.confirm({
        message: 'Are you sure you want to upload this app?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.adminService.uploadApp(this.newApp)
          .pipe(
            finalize(() => {
            })
          )
          .subscribe(app => {
            this.apps.push(app);
            this.newApp = null;
          });
        }
      });
    }
  }
}
