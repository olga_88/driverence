import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from '../shared/error404/error404.component';
import { AlertsComponent } from './alerts/alerts.component';
import { AppListComponent } from './app-list/app-list.component';
import { CallListComponent } from './call-list/call-list.component';
import { DataPrefetchGuard } from './data-prefetch.guard';

import { DriverListComponent } from './driver-list/driver-list.component';
import { LiveComponent } from './live/live.component';
import { LoadingComponent } from './loading/loading.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { MapComponent } from './map/map.component';
import { MessagesComponent } from './messages/messages.component';
import { OrderListComponent } from './order-list/order-list.component';
import { RecordListComponent } from './record-list/record-list.component';
import { TripListComponent } from './trip-list/trip-list.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [DataPrefetchGuard],
    children: [
      {
        path: '',
        redirectTo: 'map',
        pathMatch: 'full'
      },
      {
        path: 'map',
        component: MapComponent,
      },
      {
        path: 'driver-list',
        component: DriverListComponent
      },
      {
        path: 'call-list',
        component: CallListComponent
      },
      {
        path: 'trip-list',
        component: TripListComponent
      },
      {
        path: 'record-list',
        component: RecordListComponent
      },
      {
        path: 'app-list',
        component: AppListComponent
      },
      {
        path: 'order-list',
        component: OrderListComponent
      },
      {
        path: 'messages',
        component: MessagesComponent
      },
      {
        path: 'alerts',
        component: AlertsComponent
      },
    ]
  },
  {
		path: 'login',
		component: LoginComponent,
  },
  
  {
    path: 'loading',
    component: LoadingComponent,
  },

	{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }