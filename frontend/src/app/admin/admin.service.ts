import * as _ from 'lodash';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// import { UserModel, DriverModel, CallModel, RecordModel } from '@app/core/model';
import { UserModel} from '@app/core/model/user.model';
import { DriverModel} from '@app/core/model/driver.model';
import { CallModel} from '@app/core/model/call.model';
import { MessageModel} from '@app/core/model/message.model';
import { DrivingStatusModel } from '@app/core/model/status.model';
import { TripModel } from '@app/core/model/trip.model';
import { RecordModel } from '@app/core/model/record.model';
import { AppModel } from '@app/core/model/app.model';
import { OrderModel } from '@app/core/model/order.model';
import { NewAppContext } from './app-list/app-list.component';
import { NewDriverContext } from './driver-list/driver-list.component';
import { TripOrderContext } from './new-trip/new-trip.component';

const routes = {
    login: () => '/public/loginAdmin',
    registerDriver: () => '/admin/drivers/register',
    getDrivers: () => '/admin/drivers',
    getCalls: () => '/admin/calls',
    getUser: (user_id: number) => `/admin/users/${user_id}`,

    getTrips: () => '/admin/trips',
    deleteTrip: (trip_id: number) => `/admin/trips/${trip_id}`,
    getActiveTrips: () => '/admin/trips/active',
    getFinishedTrips: () => '/admin/trips/finished',
    approval: (trip_id: number) => `/admin/trips/${trip_id}/approval`,

    getMessages: (type: string) => `/admin/messages?type=${type}`,
    deleteMessages: () => '/admin/messages',
    deleteMessage: (message_id: number) => `/admin/messages/${message_id}`,

    uploadCall: () => '/admin/calls',
    updateCall: (call_id: number) =>  `/admin/calls/${call_id}`,
    deleteCall: (call_id: number) => `/admin/calls/${call_id}`,

    startLive: () => `/admin/live/start`,
    getStatuses: () => `/admin/statuses`,
    
    getRecords: () => '/admin/records',
    deleteRecord: (record_id: number) => `/admin/records/${record_id}`,

    setCallDuration: () => '/admin/calls/duration',
    getCallDuration: () => '/admin/calls/duration',

    getApps: () => '/admin/apps',
    deleteApp: (app_id: number) => `/admin/apps/${app_id}`,
    uploadApp: () => '/admin/apps/upload',

    getOrders: () => '/admin/orders',
    createOrder: () => '/admin/orders',
    cancelOrder: (order_id: number) => `/admin/orders/${order_id}/cancel`,
    deleteOrder: (order_id: number) => `/admin/orders/${order_id}`,
}

export interface LoginContext {
    email: string;
    password: string;
}

export interface ApprovalContext {
    approved: boolean,
    reject_reason?: string
}

export interface UploadDialogContext {
  message: string;
  answerRequired: boolean;
  file: File;
}

export interface UploadCallContext {
  summary: string;
  dialogs: UploadDialogContext[];
}

export interface UpdateCallContext {
  driver_ids: number[];
  is_default: boolean;
}

export interface StartLiveContext {
  driver_id: number;
  stream_type: string;
}

export interface UserDataContext {
  user_name: string;
  password: string;
}

@Injectable()
export class AdminService {
  constructor(private httpClient: HttpClient) {}

  login(context: LoginContext): Observable<any> {
    return this.httpClient.post(routes.login(), context);
  }

  registerDriver(context: NewDriverContext): Observable<DriverModel> {
    return this.httpClient.post<DriverModel>(routes.registerDriver(), context)
    .pipe(
      map((res: any) => DriverModel.fromJson(res))
    )
  }

  getDrivers(): Observable<DriverModel[]> {
    return this.httpClient.get<DriverModel[]>(routes.getDrivers())
      .pipe(
        map((res: any[]) => DriverModel.fromJsonArray(res))
      )
  }

  getCalls(): Observable<CallModel[]> {
    return this.httpClient.get(routes.getCalls())
      .pipe(
        map((res: any[]) => CallModel.fromJsonArray(res))
      );
  }

  uploadCall(context: UploadCallContext): Observable<CallModel> {
    let formData: FormData = new FormData();
    let dialogs: any = [];
    
    _.forEach(context.dialogs, ({message, file, answerRequired}, index) => {
      const fieldname = `file${index}`;
      formData.append(fieldname, file, file.name);
      dialogs.push({
        message,
        answerRequired,
        fieldname
      });
    });

    formData.append('dialogs', JSON.stringify(dialogs));
    formData.append('summary', context.summary);
    let header: HttpHeaders = new HttpHeaders();
    header.set('Content-Type', 'multipart/form-data');

    return this.httpClient.post(routes.uploadCall(), formData, {headers: header})
      .pipe(
        map((res: any) => CallModel.fromJson(res))
      );
  }

  updateCall(call_id: number, context: UpdateCallContext): Observable<CallModel> {
    return this.httpClient.put(routes.updateCall(call_id), context)
      .pipe(
        map((res: any) => CallModel.fromJson(res))
      );
  }

  deleteCall(call_id: number): Observable<any> {
    return this.httpClient.delete(routes.deleteCall(call_id));
  }

  getTrips(): Observable<TripModel[]> {
    return this.httpClient.get(routes.getTrips())
      .pipe(
        map((res: any[]) => TripModel.fromJsonArray(res))
      );
  }

  getActiveTrips(): Observable<TripModel[]> {
    return this.httpClient.get(routes.getActiveTrips())
      .pipe(
        map((res: any[]) => TripModel.fromJsonArray(res))
      );
  }

  getFinishedTrips(): Observable<TripModel[]> {
    return this.httpClient.get(routes.getFinishedTrips())
      .pipe(
        map((res: any[]) => TripModel.fromJsonArray(res))
      );
  }

  deleteTrip(trip_id: number): Observable<any> {
    return this.httpClient.delete(routes.deleteTrip(trip_id));
  }


  approval(trip_id: number, context: ApprovalContext): Observable<any> {
		return this.httpClient.put(routes.approval(trip_id), context);
  }

  getMessages(type): Observable<MessageModel[]> {
    return this.httpClient.get(routes.getMessages(type))
      .pipe(
        map((res: any[]) => MessageModel.fromJsonArray(res))
      );
  }
  
  deleteMessage(message_id: number): Observable<any> {
    return this.httpClient.delete(routes.deleteMessage(message_id))
    .pipe(
      map((res: any) => res['success'])
    );
  }

  deleteMessages(message_ids: number[]): Observable<boolean> {
    return this.httpClient.request('delete', routes.deleteMessages(), {
      body: { ids: message_ids }
    }).pipe(
      map((res: any) => res['success'])
    );
  }

  startLive(context: StartLiveContext): Observable<any> {
    return this.httpClient.post(routes.startLive(), context);
  }

  getStatuses(): Observable<DrivingStatusModel[]> {
    return this.httpClient.get(routes.getStatuses())
    .pipe(
      map((res: any[]) => DrivingStatusModel.fromJsonArray(res))
    );
  }
  
  getRecords(): Observable<RecordModel[]> {
    return this.httpClient.get(routes.getRecords())
    .pipe(
      map((res: any[]) => RecordModel.fromJsonArray(res))
    );
  }

  deleteRecord(record_id: number): Observable<any> {
    return this.httpClient.delete(routes.deleteRecord(record_id));
  }

  setCallDuration(duration: number): Observable<any> {
    return this.httpClient.post(routes.setCallDuration(), {
      duration
    }).pipe(
      map((res: any) => {
        let { duration } = res;
        return Number(duration);
      })
    );
  }

  getCallDuration(): Observable<number> {
    return this.httpClient.get(routes.getCallDuration())
    .pipe(
      map((res: any) => {
        let { duration } = res;
        return Number(duration);
      })
    );
  }

  getUser(user_id: number): Observable<UserDataContext> {
    return this.httpClient.get(routes.getUser(user_id))
    .pipe(
      map((res: any) => {
        let {user_name, password} = res;
        return {
          user_name, password
        }
      })
    );
  }

  getApps() : Observable<AppModel[]> {
    return this.httpClient.get(routes.getApps())
    .pipe(
      map((res: any[]) => AppModel.fromJsonArray(res))
    );
  }

  deleteApp(app_id: number) : Observable<any> {
    return this.httpClient.delete(routes.deleteApp(app_id));
  }

  uploadApp(context: NewAppContext) : Observable<AppModel> {
    let formData: FormData = new FormData();
    
    formData.append('summary', context.summary);
    formData.append('version', context.version);
    formData.append('file', context.file, context.file.name);
    
    let header: HttpHeaders = new HttpHeaders();
    header.set('Content-Type', 'multipart/form-data');

    return this.httpClient.post(routes.uploadApp(), formData, {headers: header})
      .pipe(
        map((res: any) => AppModel.fromJson(res))
      );
  }

  getOrders() : Observable<OrderModel[]> {
    return this.httpClient.get(routes.getOrders())
    .pipe(
      map((res: any[]) => OrderModel.fromJsonArray(res))
    );
  }

  createOrder(context: TripOrderContext) : Observable<OrderModel> {
    return this.httpClient.post<OrderModel>(routes.createOrder(), context)
    .pipe(
      map((res: any) => OrderModel.fromJson(res))
    )    
  }

  cancelOrder(order_id: number) : Observable<OrderModel> {
    return this.httpClient.post(routes.cancelOrder(order_id), {})
    .pipe(
      map((res: any) => OrderModel.fromJson(res))
    );
  }

  deleteOrder(order_id: number) : Observable<any> {
    return this.httpClient.delete(routes.deleteOrder(order_id));
  }
}
