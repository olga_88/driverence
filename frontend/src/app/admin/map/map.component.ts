import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SocketService } from '@app/core/socket/socket.service';

import { DriverModel, LocationModel, RecordModel, TripModel } from '@core/model';
import { AdminService } from '../admin.service';

import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { LiveService, StreamType } from '@app/core/live.service';
import { DrivingStatusModel, StatusTag } from '@app/core/model/status.model';
import { environment } from '@env/environment';

export interface MapViewModel {
  driver_id: number;
  full_name: string;
  info: DriverModel;
  trip: TripModel;
  location: LocationModel;
  status: DrivingStatusModel;
  marker: google.maps.Marker;
  markerInfo: google.maps.InfoWindow;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  filters: any[] = [
    { name: 'Available', value: 'normal', icon: "fa fa-stop-circle-o"},
    { name: 'Waiting for', value: 'waiting_approval', icon: "fa fa-spinner"},
    { name: 'On trip', value: 'on_trip', icon: "fa fa-car"},
    { name: 'Emergency', value: 'emergency', icon:"fa fa-exclamation-circle"}
  ];
  selectedFilters: any[] = [];

  subscription: Subscription = new Subscription();
  map: google.maps.Map;
  
  data: MapViewModel[] = [];

  driverListData: MapViewModel[] = [];
  mapOverlayData: google.maps.Marker[] = [];

  selectedViewModel: MapViewModel = null;

  displaySidebar: boolean = false;

  constructor(
    private adminService: AdminService, 
    private socketService: SocketService,
    private route: ActivatedRoute,
    private liveService: LiveService) {     
  }

  get mapOverlays() {
    return this.mapOverlayData;
  }

  get mapOptions() {
    return {
      center: {lat: 36.890257, lng: 30.707417},
      zoom: 12
    };
  }

  private getVisibility(viewModel: MapViewModel): boolean {
    let emergencyOnly = this.selectedFilters.indexOf('emergency') >= 0;
    let showAll = this.selectedFilters.length === 0 || this.selectedFilters.length === 3;
    
    if(showAll) return true;
    else if(emergencyOnly) {
      return viewModel.status && viewModel.status.tag !== StatusTag.Normal;
    } else {
      return this.selectedFilters.indexOf(viewModel.info.status) >= 0;
    }
  }

  private refreshOne(viewModel: MapViewModel) {
    let visible = this.getVisibility(viewModel);

    let { driver_id } = viewModel;

    if(visible) {
      if(!_.find(this.driverListData, {driver_id})) {
        this.driverListData.push(viewModel);
      }

      if(!_.find(this.mapOverlayData, marker => marker.get('driver_id') === driver_id)) {
        this.mapOverlayData.push(viewModel.marker);
      }
    } else {
      _.remove(this.driverListData, {driver_id});
      _.remove(this.mapOverlayData, marker => marker.get('driver_id') === driver_id);
    }
  }

  private refreshAll() {
    // Visible validation    
    this.driverListData = _.filter(this.data, d => this.getVisibility(d));
    this.mapOverlayData = this.driverListData.map(v => v.marker).filter(m => m);
  }

  private generateViewModelFromDriver(driver: DriverModel): MapViewModel {
    let driver_id = driver.driver_id;
    let location = _.find(this.socketService.locations, { driver_id });
    let status = _.find(this.socketService.statuses, { driver_id });
    let trip = _.find(this.socketService.activeTrips, { driver_id });
    let record = _.find(this.socketService.lastRecords, {driver_id });
    
    let marker: google.maps.Marker = null;
    let markerInfo: google.maps.InfoWindow = null;
    if(location) {
      marker = new google.maps.Marker({
        position: { lat: location.latitude, lng: location.longitude},
        // title: driver.full_name,
        // label: driver.full_name
      });
      marker.set('driver_id', driver.driver_id);

      if(record && record.analysis_report) {
        markerInfo = new google.maps.InfoWindow({
          content: `
          <div class="record-info">
            <div class="header">${driver.full_name}</div>

            <div class="content">
              <div>
                <img src="${record.analysis_report.tags.length == 0 ? 
                  "assets/layout/images/check-circle.png" : 
                  "assets/layout/images/alert-circle.png"}" />
              </div>
              <div>
                <div>Stress: ${record.analysis_report.Stress}</div>
                <div>Energy: ${record.analysis_report.Energy}</div>
              </div>
            </div>
            
            <div class="footer">${record.analysis_report.tags.join(', ')}</div>
          </div>
          `
        });
      } else {
        markerInfo = new google.maps.InfoWindow({
          content: `
          <div><b>${driver.full_name}</b></div>
          `
        });
      }
      markerInfo.open(this.map, marker);

      marker.set('infoWindow', markerInfo);
    }
    
    return {
      driver_id: driver.driver_id,
      full_name: driver.full_name,
      info: driver,
      trip,
      location,
      status,
      marker,
      markerInfo
    };
  }

  private getViewModelById(driver_id: number) {
    return _.find(this.data, { driver_id });
  }


  private onDriverUpdate(driver: DriverModel) {
    let viewModel = this.getViewModelById(driver.driver_id);

    if(viewModel) {
      viewModel.info = driver;
      viewModel.full_name = driver.full_name;      
    } else {
      viewModel = this.generateViewModelFromDriver(driver);
      this.data.push(viewModel);
    }

    this.refreshOne(viewModel);
  }

  private onLocationUpdate(location: LocationModel) {
    // modify marker
    let viewModel = this.getViewModelById(location.driver_id);

    if(!viewModel) return;

    viewModel.marker.setPosition({
      lat: location.latitude,
      lng: location.longitude
    });
  }

  private onRecordUpdate(record: RecordModel) {
    // modify marker info
    let viewModel = this.getViewModelById(record.driver_id);

    if(!viewModel) return;

    if(record.analysis_report == null) return;

    viewModel.markerInfo.setContent(`
    <div class="record-info">
        <div class="header">${viewModel.full_name}</div>

        <div class="content">
          <div>
            <img src="${record.analysis_report.tags.length == 0 ? 
              "assets/layout/images/check-circle.png" : 
              "assets/layout/images/alert-circle.png"}" />
          </div>
          <div>
            <div>Stress: ${record.analysis_report.Stress}</div>
            <div>Energy: ${record.analysis_report.Energy}</div>
          </div>
        </div>
        
        <div class="footer">${record.analysis_report.tags.join(', ')}</div>
      </div>
    `);
  }

  private onStatusUpdate(status: DrivingStatusModel) {
    let viewModel = this.getViewModelById(status.driver_id);
    if(!viewModel) return;
    viewModel.status = status;
    this.refreshOne(viewModel);
  }

  
  ngOnInit(): void {
    // init view models
    this.data = _.map(this.socketService.drivers, driver => this.generateViewModelFromDriver(driver));

    this.refreshAll();

    this.subscription.add(
      this.socketService.lastDriverObservable
        .subscribe(driver => this.onDriverUpdate(driver))
    );

    this.subscription.add(
      this.socketService.lastLocationObservable
        .subscribe(location => this.onLocationUpdate(location))
    );

    this.subscription.add(
      this.socketService.lastStatusObservable
        .subscribe(status => this.onStatusUpdate(status))
    );

    this.subscription.add(
      this.socketService.lastRecordObservable
        .subscribe(record => this.onRecordUpdate(record))
    );


    // Focus
    this.route.queryParams.subscribe(params => {
      let {driver_id} = params;
      if(driver_id) 
        this.focusDriver(driver_id);
    });
    
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private updateCameraBounds() {
    if(!this.map) return;

    
    // initialize camera
    let bounds = new google.maps.LatLngBounds();
    let allBounds = new google.maps.LatLngBounds();
    let showPart = false;
    _.forEach(this.mapOverlayData, marker => {
      allBounds.extend(marker.getPosition());

      if(marker.getVisible()) {
        bounds.extend(marker.getPosition());
        showPart = true;
      }
    });

    if(showPart) {
      this.map.fitBounds(bounds);
    } else {
      this.map.fitBounds(allBounds);
    }
  }

  private focusDriver(driver_id: number) {
    if(!this.map) return;

    let viewModel = this.getViewModelById(driver_id);

    if(!viewModel) return;

    _.forEach(this.mapOverlayData, marker => {
      marker.setZIndex(0)
      marker.get('infoWindow').setZIndex(0);
    });
    
    let {marker, markerInfo} = viewModel;

    if(marker) {
      this.map.setCenter(marker.getPosition());
      marker.setZIndex(1);
    }

    if(markerInfo) {
      markerInfo.setZIndex(1);
    }
  }

  private showDriverInfo(driver_id: number) {
    this.selectedViewModel = this.getViewModelById(driver_id);

    if(this.selectedViewModel) {
      this.displaySidebar = true;
    }
  }

  // UI Events
  onMapReady(event) {
    this.map = event.map;

    this.updateCameraBounds();
  }

  onOpenLiveStream(viewModel: MapViewModel) {
    if(viewModel) {
      this.liveService.startStream(StreamType.FrontCamera, viewModel.info.driver_id);
      this.displaySidebar = false;
    }
  }

  onOpenUserData(viewModel: MapViewModel) {
    if(viewModel) {
      let driver = _.find(this.socketService.drivers, { driver_id: viewModel.driver_id });

      if(driver) {
        this.adminService.getUser(driver.user_id).subscribe(data => {
          this.post({
            username: data.user_name,
            password: data.password,
            ish: 1
          }, environment.userLoginUrl);
        });
      }
    }
  }

  post(obj: Object,url: string) {
    var mapForm = document.createElement("form");
    mapForm.target = "_blank";
    mapForm.method = "POST"; // or "post" if appropriate
    mapForm.action = url;
    Object.keys(obj).forEach(function(param){
      var mapInput = document.createElement("input");
      mapInput.type = "hidden";
      mapInput.name = param;
      mapInput.setAttribute("value", obj[param]);
      mapForm.appendChild(mapInput);
    });
    document.body.appendChild(mapForm);
    mapForm.submit();
  }

  onSelectedFilterChanged(event) {
    // XOR emergency
    let lastSelected = _.last(this.selectedFilters);
    if(lastSelected && lastSelected === 'emergency') {
      this.selectedFilters = ['emergency'];
    } else {
      _.remove(this.selectedFilters, s => s === 'emergency');
    }

    this.refreshAll();
  }

  onClearFilters(event: any) {
    if(this.selectedFilters.length === 0) return;

    this.selectedFilters = [];
    this.refreshAll();
  }

  onDriverListSelectionChanged(event) {
    let driver_id = event.value;
    this.focusDriver(driver_id);
  }

  onMapOverlayClicked(event) {
    //event.originalEvent: MouseEvent of Google Maps api
    //event.overlay: Clicked overlay
    //event.map: Map instance

    let {driver_id} = event.overlay;
    this.showDriverInfo(driver_id);
  }


  // UI Utils
  firstLetter(driver: DriverModel) {
    return driver.first_name[0].toUpperCase() + driver.last_name[0].toUpperCase();
  }

  getStatusBadge(status: string) {
    let badges = {
      normal: {
        label: 'Active',
        badgeStyle: 'text-badge badge-cyan'
      },
      waiting_approval: {
        label: 'Waiting for approval',
        badgeStyle: 'text-badge badge-violet'
      },
      on_trip: {
        label: 'On trip',
        badgeStyle: 'text-badge badge-emerald'
      },
      unknown: {
        label: 'Unknown',
        badgeStyle: 'text-badge badge-olive'
      }
    };

    return badges[status] || badges['unknown'];
  }


  getTagBadge(tag: string) {
    let badges = {};

    badges[StatusTag.Normal] = {
      label: 'Normal',
      badgeStyle: 'text-badge badge-cyan'
    };

    badges[StatusTag.ConnectionLost] = {
      label: 'Connection Lost',
      badgeStyle: 'text-badge badge-yellow'
    };
    
    badges[StatusTag.Stopped] = {
      label: 'Stopped',
      badgeStyle: 'text-badge badge-olive'
    };

    badges[StatusTag.WrongWay] = {
      label: 'Wrong Way',
      badgeStyle: 'text-badge badge-orange'
    };

    badges[StatusTag.Emergency] = {
      label: 'Emergency',
      badgeStyle: 'text-badge badge-magenta'
    }

    badges['unknown'] = {
      label: 'Unknown',
      badgeStyle: 'text-badge badge-olive'
    }

    return badges[tag] || badges['unknown'];
  }
}
