import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageModel } from '@app/core/model';
import { NgProgressComponent } from 'ngx-progressbar';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;
  
  messages: MessageModel[];
  selectedMessages: MessageModel[] = [];
  menuitems: MenuItem[] = [
    {label:'Notification', icon: 'pi pi-fw pi-inbox'},
    {label:'Messages', icon: 'pi pi-fw pi-comment'}
  ];

  constructor(private adminService: AdminService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.getMessages();
  }

  
  getMessages() {
    this.adminService.getMessages('log')
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      messages => {
        this.messages = messages;
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  
  getBadge(category) {
    let badges = {
      driver_register: {
        label: 'Registered',
        badgeStyle: 'text-badge badge-cyan'
      },
      driving_request: {
        label: 'Request',
        badgeStyle: 'text-badge badge-violet'
      },
      approved: {
        label: 'Approved',
        badgeStyle: 'text-badge badge-emerald'
      },
      rejected: {
        label: 'Rejected',
        badgeStyle: 'text-badge badge-magenta'
      },
      unknown: {
        label: 'Unknown',
        badgeStyle: 'text-badge badge-olive'
      }
    };

    
    return badges[category] || badges['unknown'];
  }

  deleteSelectedMessages() {
    this.confirmationService.confirm({
        message: `Are you sure you want to delete the selected (${this.selectedMessages.length}) messages?`,
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            let ids = this.selectedMessages.map(m => m.message_id);
            this.adminService.deleteMessages(ids).subscribe(success => {
              if(success) {
                _.pullAll(this.messages, this.selectedMessages);

                this.selectedMessages = [];
              }
            });
        }
    });
  }
}
