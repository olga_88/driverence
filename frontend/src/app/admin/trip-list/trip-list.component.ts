import { Component, OnInit, ViewChild } from '@angular/core';
import { NgProgressComponent } from 'ngx-progressbar';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';

import * as _ from 'lodash';
import { TripModel } from '@app/core/model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-trip-list',
  templateUrl: './trip-list.component.html',
  styleUrls: ['./trip-list.component.scss']
})
export class TripListComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;
  
  options = [
    { label: 'All', value: 'all' },
    // { label: 'Pending', value: 'pending'},
    { label: 'On-going', value: 'active'},
    { label: 'Finished', value: 'finished'}
  ];

  selectedOption:string = 'all';

  trips: TripModel[];

  menuitems: MenuItem[] = [
    {label:'Management', icon: 'pi pi-fw pi-list'},
    {label:'Trips', icon: 'pi pi-fw pi-directions'}
  ];


  constructor(private adminService: AdminService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.getTrips();
  }

  getTrips() {
    let sub: Observable<TripModel[]>;

    switch(this.selectedOption) {
      case 'active':
        sub = this.adminService.getActiveTrips(); break;
      case 'finished':
        sub = this.adminService.getFinishedTrips(); break;
      default: 
        sub = this.adminService.getTrips(); break;
    }

    sub
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      trips => {
        this.trips = trips;
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  onDeleteTrip(trip: TripModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this trip?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.adminService.deleteTrip(trip.trip_id).subscribe(
            res => {
              _.remove(this.trips, { trip_id: trip.trip_id });
            },
            error => {
              let msg = error.error?.msg || error.message;
              this.error = msg;
            });
      }
    });
  }

  onSelectedOptionChanged(event) {
    this.getTrips();
  }
}
