import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { ConfirmationService, MenuItem, Message, MessageService } from 'primeng/api';

import { MessageModel, TripModel, DriverModel, RecordModel } from '@core/model';

import * as moment from 'moment';
import * as _ from 'lodash';

import { SocketService } from '@app/core/socket/socket.service';
import { Subscription } from 'rxjs';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { CommandType, LiveService, StreamType } from '@app/core/live.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { LiveComponent } from '../live/live.component';
import { NewTripComponent } from '../new-trip/new-trip.component';

export interface PendingTripViewModel {
  driver_id: number;
  driver: DriverModel;
  trip: TripModel;
  record: RecordModel;
};

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  error: string;

  menu: MenuItem[] = [
    {
      label: 'Map',
      icon: 'pi pi-fw pi-compass',
      routerLink: '/admin/map'
    },
    {
      label: 'Management',
      icon: 'pi pi-list',
      items: [
        {
          label: 'Drivers',
          icon: 'pi pi-fw pi-users',
          routerLink: '/admin/driver-list'
        },
        {
          label: 'Questions',
          icon: 'pi pi-fw pi-volume-up',
          routerLink: '/admin/call-list'
        },
        {
          label: 'Trips',
          icon: 'pi pi-fw pi-directions',
          routerLink: '/admin/trip-list'
        },
        {
          label: 'Trip Orders',
          icon: 'pi pi-fw pi-calendar',
          routerLink: '/admin/order-list'
        },
        {
          label: 'Records',
          icon: 'pi pi-fw pi-video',
          routerLink: '/admin/record-list'
        },
        {
          label: 'Apps',
          icon: 'pi pi-cloud-upload',
          routerLink: '/admin/app-list'
        }
      ],
    },

    {
      label: 'Notification',
      icon: 'pi pi-inbox',
      items: [
        {
          label: 'Messages',
          icon: 'pi pi-fw pi-comment',
          routerLink: '/admin/messages'
        },
        {
          label: 'Alerts',
          icon: 'pi pi-fw pi-bell',
          routerLink: '/admin/alerts'
        }
      ]
    }
  ];

  messages: MessageModel[] = [];
  alerts: MessageModel[] = [];

  pendingTrips: PendingTripViewModel[] = [];

	subscription: Subscription = new Subscription();
	
	liveRef: DynamicDialogRef;
  liveOpen: boolean = false;

  newTripRef: DynamicDialogRef;
  newTripOpen: boolean = false;

  lastMessageTimstamp: Date = new Date();
  lastAlertTimestamp: Date = new Date();

  constructor(
    private authService: AuthenticationService,
    private socketService: SocketService,
    private adminService: AdminService,
		private confirmationService: ConfirmationService,
		private messageService: MessageService,
		private router: Router,
		private liveService: LiveService,
		private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.subscription.add(
      this.socketService.lastMessageObservable.subscribe(message => {
        if(message.type === 'log') {
          this.messages.unshift(message);
        } else if(message.type === 'alert') {
          if(message.category === 'sos') {
            if(this.liveOpen) {
              this.showSos(message);
            } else {
              this.startLivestream(message);
            }
          }
          this.alerts.unshift(message);
        }
      })
    );

    this.subscription.add(
      this.socketService.pendingTripsObservable.subscribe(pendingTrips => {
        this.pendingTrips = [];

        this.pendingTrips = _.map(pendingTrips, trip => {
          let driver_id = trip.driver_id;
          return {
            driver_id,
            driver: _.find(this.socketService.drivers, {driver_id}),
            trip,
            record: _.find(this.socketService.lastRecords, { driver_id })
          };
        });
      })
		);

    this.subscription.add(
      this.socketService.lastRecordObservable.subscribe(record => {
        let driver_id = record.driver_id;

        let trip = _.find(this.pendingTrips, {driver_id});
        if(trip) {
          trip.record = record;
        }
      })
    );
		
		this.subscription.add(
			this.liveService.commandObservable.subscribe(command => {
				if (command.type === CommandType.Start) {
          let driver = _.find(this.socketService.drivers, {driver_id: command.driver_id});

					// Start LiveStream
					this.liveRef = this.dialogService.open(LiveComponent, {
						header: `Emergency Live Stream (${driver.full_name})`,
						width: '640px',
						data: {
							driver_id: command.driver_id,
							stream_type: command.stream_type
						}
					});

          this.liveRef.onClose.subscribe(() => {
            this.liveOpen = false;
          });

          this.liveOpen = true;
				} else if(command.type === CommandType.Stop) {
					if(this.liveRef) this.liveRef.close();
				}
			})
		)
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  get username() {
    let {first_name, last_name} = this.authService.credentials.user;

    if(!first_name) first_name = "";
    if(!last_name) last_name = "";
    
    return `${first_name} ${last_name}`;
  }

  get firstLetter() {
    let {first_name, last_name} = this.authService.credentials.user;

    if(!first_name) first_name = " ";
    if(!last_name) last_name = " ";

    return `${first_name[0]}${last_name[0]}`
  }

  onHideMessages() {
    this.lastMessageTimstamp = new Date();
  }

  onHideAlerts() {
    this.lastAlertTimestamp = new Date();
  }

  get newMessageCount() {
    return _.filter(this.messages, message => this.isNewMessage(message)).length;
  }

  get newAlertCount() {
    return _.filter(this.alerts, alert => this.isNewAlert(alert)).length;
  }

  isNewMessage(message: MessageModel) {
    return moment(message.created_at).isAfter(this.lastMessageTimstamp);
  }

  isNewAlert(alert: MessageModel) {
    return moment(alert.created_at).isAfter(this.lastAlertTimestamp);
  }

  onApprove(trip: PendingTripViewModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you approve this trip?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.adminService.approval(trip.trip.trip_id, {
          approved: true
        }).subscribe(
            res => {
              
            },
            error => {
              let msg = error.error?.msg || error.message;
              this.error = msg;
        });
      }
    });
  }

  onReject(trip: PendingTripViewModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you reject this trip?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.adminService.approval(trip.trip.trip_id, {
          approved: false,
          reject_reason: trip.record.analysis_result
        }).subscribe(
            res => {
            },
            error => {
              let msg = error.error?.msg || error.message;
              this.error = msg;
        });
      }
    });
  }

  onNewOrder() {
    this.newTripRef = this.dialogService.open(NewTripComponent, {
      header: `Create Trip Order`,
    });

    this.newTripRef.onClose.subscribe(() => {
      this.newTripOpen = false;
    });

    this.newTripOpen = true;
  }

	showSos(message: MessageModel) {
		this.messageService.add({
			key: 'sos', 
			sticky: true, 
			severity:'warn', 
			summary: message.content, 
			detail: `Show driver's location on the map`,
			data: message
		});
	}

  startLivestream(message: MessageModel) {
    let {driver_id} = message.meta_data;

    this.liveService.startStream(StreamType.FrontCamera, driver_id);
  }


  onConfirmSos(message: Message) {
		this.messageService.clear('sos');

		let data = message.data as MessageModel;
		let {driver_id} = data.meta_data;

		this.navigateToMap(driver_id);
	}

  onRejectSos() {
		this.messageService.clear('sos');
	}

  onSignoutClicked() {
    this.confirmationService.confirm({
      message: 'Are you sure to sign out?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.authService.logout();
        this.router.navigateByUrl('/admin/login');
      }
    });
  }

	navigateToMap(driver_id: number) {
		this.router.navigate(["/admin/map"], {queryParams: {
			driver_id
		}});
	}
}
