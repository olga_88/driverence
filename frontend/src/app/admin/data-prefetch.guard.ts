import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { SocketService } from "@app/core/socket/socket.service";
import { Observable } from "rxjs";

@Injectable()
export class DataPrefetchGuard implements CanActivate {
  constructor(
    private socketSerivce: SocketService,
    private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
      return new Observable<boolean>(obs => {
        this.socketSerivce.prefetchCompletedObservable.subscribe(completed => {
          if(completed) {
            obs.next(true);
          } else {
            this.router.navigate(['/admin/loading'], {
              queryParams: { redirect: state.url },
              replaceUrl: true
            });
            obs.next(false);
          }
        });
      });
    
  }
}