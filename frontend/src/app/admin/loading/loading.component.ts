import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SocketService } from '@app/core/socket/socket.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(
    private socketService: SocketService, 
    private route: ActivatedRoute, 
    private router: Router) { }

  ngOnInit(): void {
    this.socketService.prefetchCompletedObservable.subscribe(completed => {
      this.route.queryParams.subscribe(params =>
        this.router.navigate([params.redirect || '/admin'], {
          replaceUrl: true
        })
      );
    });
    
  }

}
