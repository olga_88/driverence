import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import * as WebRtcAdapter from 'webrtc-adapter';
import { AdminService } from '../admin.service';
import { environment } from '@env/environment';
import { ActivatedRoute } from '@angular/router';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

const TYPE_CALL = "call";
const TYPE_ANSWER = "answer";
const TYPE_LOCAL_CANDIDATE = "localIceCandidate";
const TYPE_REMOTE_CANDIDATE = "remoteIceCandidate";
const TYPE_ERROR = "error";
const TYPE_REMOTE_SDP = "sdp";


@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit, OnDestroy {
  @ViewChild("video") videoView: ElementRef;

  driver_id: number;
  stream_type: string;

  ws: WebSocket;
  peerConnection: RTCPeerConnection;
  peerConnectionConfig = {
    'iceServers': [
        environment.iceServer
    ]
  };

  isStreaming: boolean = false;


  error: string;

  constructor(
    private adminService: AdminService,
    private route: ActivatedRoute,
    private dialogRef: DynamicDialogRef,
    private dialogConfig: DynamicDialogConfig
  ) {
    
  }

  ngOnInit(): void {
    // console.log(WebRtcAdapter.default.browserDetails);
    if(this.dialogConfig) {
      let { driver_id, stream_type } = this.dialogConfig.data;

      this.driver_id = driver_id;
      this.stream_type = "cam1";

      this.startLive();
    }
  }

  ngOnDestroy(): void {
    this.stopCall();
  }

  get videoElement() {
    return this.videoView.nativeElement;
  }

  toggle() {
    console.log(`old stream type: ${this.stream_type}`);

    if(this.stream_type === 'cam1') {
      this.stream_type = 'cam2';
    } else if(this.stream_type === 'cam2') {
      this.stream_type = 'cam1';
    }

    console.log(`new stream type: ${this.stream_type}`);

    this.stopCall();

    setTimeout(() => {
      this.startLive();
    }, 3000);
  }

  startLive() {
    this.adminService.startLive({
      driver_id: this.driver_id,
      stream_type: this.stream_type
    })
    .pipe(
      finalize(() => {
      })
    )
    .subscribe(
      res => {
        let {room_id} = res;
        console.log(res);
        this.startCall(room_id);
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  startCall(roomId: string) {
    console.log(`Start call`);

    let sessionId = 'testsessionAngular';
    let ws = new WebSocket(`${environment.signalServer}/join/${roomId}?sessionId=${sessionId}`);

    ws.onopen = () => {
      console.info(`WS connection success`);
    };

    ws.onclose = (ev: CloseEvent) => {
      console.info(`WS disconnected, code = ${ev.code}, reason = ${ev.reason}`);
      if(ev.code === 1007) {
        this.error = "Connectino already exists, retry after few seconds";
      }

      this.stopCall();
    }

    ws.onerror = (ev) => {
      console.error(`WS error`, ev);
      this.stopCall();
    }

    ws.onmessage = async (message) => {
      var json;
      try {
          json = JSON.parse(message.data);
      } catch (e) {
          console.error(`Failed to parse WS message`);
          return;
      }

      switch (json.type) {
          case TYPE_ERROR: {
              /** @type {{code: number, message: string}} */
              const {code, message} = json;
              if(message) {
                  console.error(`${message}`);
              }
              break;
          }
          case TYPE_REMOTE_CANDIDATE: {
              const {candidate} = json;

              console.log(`Add remote candidate`, candidate);
              await this.peerConnection.addIceCandidate(candidate);
              break;
          }
          case TYPE_REMOTE_SDP: {
              this.peerConnection = new RTCPeerConnection(this.peerConnectionConfig);
              this.peerConnection.onicecandidate = ({candidate}) => {
                  if(candidate) {
                      // console.log(`Got ice-candidate,`, candidate);
                      ws.send(JSON.stringify({
                          type: TYPE_LOCAL_CANDIDATE,
                          candidate: candidate
                      }));
                  }
              };
              this.peerConnection.ontrack = (event) => {
                  console.log(`Got remote stream`, event.streams[0]);
                  this.videoElement.srcObject = event.streams[0];

                  this.isStreaming = true;
              };
      
              /** @type {{sdpType: string, sdp: string}} */
              const {sdpType: type, sdp}= json;
              if(type === 'offer') {
                  console.log(`[Offer] Set remote description`);
                  await this.peerConnection.setRemoteDescription({
                      type,
                      sdp
                  });
                  
                  const answer = await this.peerConnection.createAnswer();
                  console.log(`Created answer`, answer);
                  await this.peerConnection.setLocalDescription(answer);
                  ws.send(JSON.stringify({
                      type: TYPE_ANSWER,
                      sdpAnswer: answer.sdp
                  }));
              } else if(type === 'answer') {
                  console.log(`[Answer] Set remote description`);
                  await this.peerConnection.setRemoteDescription({
                      type,
                      sdp
                  });
              }
              break;
          }
      }
    }

    this.ws = ws;
  }

  stopCall() {
    if(this.peerConnection) {
      this.peerConnection.close();
      this.peerConnection = null;
    }

    if(this.ws) {
      this.ws.close();
      this.ws = null;
    }

    this.isStreaming = false;
  }
}
