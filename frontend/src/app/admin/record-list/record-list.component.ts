import * as _ from 'lodash';

import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { RecordModel } from '@app/core/model';
import { NgProgressComponent } from 'ngx-progressbar';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import { VideoComponent } from '../video/video.component';

import { saveAs } from 'file-saver';
import { SocketService } from '@app/core/socket/socket.service';

@Component({
  selector: 'app-record-list',
  templateUrl: './record-list.component.html',
  styleUrls: ['./record-list.component.scss']
})
export class RecordListComponent implements OnInit {
  error: string;
  @ViewChild(NgProgressComponent) progressBar: NgProgressComponent;
  
  videoRef: DynamicDialogRef;
  records: RecordModel[];

  menuitems: MenuItem[] = [
    {label:'Management', icon: 'pi pi-fw pi-list'},
    {label:'Records', icon: 'pi pi-fw pi-video'}
  ];

  constructor(
    private socketService: SocketService,
    private adminService: AdminService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private dialogService: DialogService,
    private santizier: DomSanitizer) { }

  ngOnInit(): void {
    this.getRecords();
  }

  
  getRecords() {
    this.adminService.getRecords()
    .pipe(
      finalize(() => {
        this.progressBar.complete();
      })
    )
    .subscribe(
      records => {
        this.records = records;
        _.forEach(this.records, record => {
          record.latest = !!_.find(this.socketService.lastRecords, { record_id: record.record_id });
        });
      },
      error => {
        let msg = error.error?.msg || error.message;
        this.error = msg;
      }
    );
  }

  getAssetUrl(path, useSantizier = true) {
    let token = this.authService.credentials.token;
    path = `/api/assets/${path}?token=${token}`
    return useSantizier ? this.santizier.bypassSecurityTrustUrl(path) : path;
  }

  onPlayRecord(record: RecordModel) {
    let videoUrl =  this.getAssetUrl(record.video_file_path);
    this.videoRef = this.dialogService.open(VideoComponent, {
      header: 'Record',
      width: '640px',
      data: {
        url: videoUrl
      }
    });
  }

  onDownloadRecord(record: RecordModel) {
    saveAs(this.getAssetUrl(record.video_file_path, false), "record.mp4");
  }

  onDeleteRecord(record: RecordModel) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this record?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        let record_id = record.record_id;
        this.adminService.deleteRecord(record_id).subscribe(
          res => {
            _.remove(this.records, { record_id });
          },
          error => {
            let msg = error.error?.msg || error.message;
            this.error = msg;
          });
      }
    });
  }
}
