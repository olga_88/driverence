import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log(state.url);

    if(state.url.startsWith("/admin/login")) {
      // Login page doesn not require AUTH
      return true;
    }

    if (this.authenticationService.isAuthenticated()) {
      return true;
    }

    this.router.navigate(['/admin/login'], {
      queryParams: { redirect: state.url },
      replaceUrl: true
    });
    return false;
  }
}
