import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'

export enum CommandType {
  Start = 'start',
  Stop = 'stop'
}

export enum StreamType {
  FrontCamera = 'cam1',
  BackCamera = 'cam2'
}

export interface CommandContext {
  type: CommandType;
  driver_id?: number;
  stream_type?: StreamType;
}

@Injectable({
  providedIn: 'root'
})
export class LiveService {
  private command: Subject<CommandContext> = new Subject();

  constructor() { }

  startStream(stream_type: StreamType, driver_id: number) {
    this.command.next({
      type: CommandType.Start,
      stream_type,
      driver_id
    });
  }

  stopStream() {
    this.command.next({
      type: CommandType.Stop
    });
  }

  get commandObservable() {
    return this.command.asObservable();
  }
}
