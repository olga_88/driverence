import * as moment from 'moment';
import * as _ from 'lodash';
import { DriverModel } from './driver.model';

export class OrderModel {
    order_id: number;
    driver_id: number;
    destination: string;
    latitude: number;
    longitude: number;
    status: string;
    created_at: Date;
    expired_at?: Date;

    driver?: DriverModel;

    static fromJson(jsonObject): OrderModel {
        let {
            order_id,
            driver_id,
            destination,
            longitude,
            latitude,
            status,
            created_at,
            expired_at
        } = jsonObject;

        return {
			order_id: Number(order_id),
            driver_id: Number(driver_id),
            destination,
            latitude: Number(latitude),
            longitude: Number(longitude),
            status,
            created_at: moment(created_at).toDate(),
            expired_at: expired_at == null ? null : moment(expired_at).toDate(),
        };
    }

    static fromJsonArray(jsonArray): OrderModel[] {
        return _.map(jsonArray, jsonObject => OrderModel.fromJson(jsonObject));
    }
}
