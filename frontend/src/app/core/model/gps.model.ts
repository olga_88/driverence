import * as moment from 'moment';
import * as _ from 'lodash';

export class GpsModel {
    gps_id: number;
    user_id: number;
    device_id: number;
    latitude: number;
    longitude: number;
    phone_time: Date;
    upload_time: Date;


    static fromJson(jsonObject): GpsModel {
        let { gps_id, user_id, device_id, latitude, longitude, phone_time, upload_time} = jsonObject;

        return {
            gps_id: Number(gps_id),
            user_id: Number(user_id),
            device_id: Number(device_id),
            latitude: Number(latitude),
            longitude: Number(longitude),
            phone_time: moment(phone_time).toDate(),
            upload_time: moment(upload_time).toDate()
        };
    }

    static fromJsonArray(jsonArray): GpsModel[] {
        return _.map(jsonArray, jsonObject => GpsModel.fromJson(jsonObject));
    }
}