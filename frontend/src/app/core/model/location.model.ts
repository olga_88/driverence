import * as _ from 'lodash';
import * as moment from 'moment';

export class LocationModel {
    driver_id: number;
    latitude: number;
    longitude: number;
    updated_at: Date;

    static fromJson(jsonObject): LocationModel {
        let {
            driver_id,
            latitude,
            longitude,
            updated_at
        } = jsonObject;

        return {
            driver_id: Number(driver_id),
            latitude: Number(latitude),
            longitude: Number(longitude),
            updated_at: moment(updated_at).toDate()
        };
    }

    static fromJsonArray(jsonArray): LocationModel[] {
        return _.map(jsonArray, jsonObject => LocationModel.fromJson(jsonObject));
    }
}