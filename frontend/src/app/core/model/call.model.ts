import * as moment from 'moment';
import * as _ from 'lodash';
import { DriverModel } from './driver.model';

export class DialogModel {
    message: string;
    answerRequired: boolean;
    file_path: string;

    safe_url?: any;

    static fromJson(jsonObject): DialogModel {
        if(!jsonObject) return undefined;
        
        let { message, answerRequired, file_path} = jsonObject;
        return {
            message,
            answerRequired: Boolean(answerRequired),
            file_path
        };
    }

    static fromJsonArray(jsonArray): DialogModel[] {
        return _.map(jsonArray, jsonObject => DialogModel.fromJson(jsonObject));
    }
}

export class CallModel {
    call_id: number;
    summary: string;
    created_at: Date;
    updated_at: Date;
    driver_ids: number[];
    is_default: boolean;
    dialogs: DialogModel[];

    drivers?: DriverModel[];

    static fromJson(jsonObject): CallModel {
        if(!jsonObject) return undefined;
        
        let {call_id, summary, driver_ids, is_default, created_at, updated_at, dialogs} = jsonObject;

        return {
            call_id: Number(call_id),
            summary,
            driver_ids,
            is_default,
            created_at: moment(created_at).toDate(),
            updated_at: moment(updated_at).toDate(),
            dialogs: DialogModel.fromJsonArray(dialogs)
        };
    }

    static fromJsonArray(jsonArray): CallModel[] {
        return _.map(jsonArray, jsonObject => CallModel.fromJson(jsonObject));
    }
}
