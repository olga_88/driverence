import * as moment from 'moment';
import * as _ from 'lodash';

export class AppModel {
    app_id: number;
    summary: string;
    version: string;
    file_path: string;
    uploaded_at: Date;

    static fromJson(jsonObject): AppModel {
        let {
            app_id,
            summary,
            version,
            file_path,
            uploaded_at
        } = jsonObject;

        return {
            app_id: Number(app_id),
            summary,
            version,
            file_path,
            uploaded_at: moment(uploaded_at).toDate()
        };
    }

    static fromJsonArray(jsonArray): AppModel[] {
        return _.map(jsonArray, jsonObject => AppModel.fromJson(jsonObject));
    }
}