import * as _ from 'lodash';

export class UserModel {
    user_id: number;
    user_name: string;
    first_name: string;
    last_name: string;
    email: string;
    phone_number: string;

    static fromJson(jsonObject): UserModel {
        let { user_id, user_name, first_name, last_name, email, phone_number} = jsonObject;

        return {
            user_id,
            user_name,
            first_name,
            last_name,
            email,
            phone_number,
        };
    }

    static fromJsonArray(jsonArray): UserModel[] {
        return _.map(jsonArray, jsonObject => UserModel.fromJson(jsonObject));
    }
}
