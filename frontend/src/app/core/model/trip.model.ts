import * as moment from 'moment';
import * as _ from 'lodash';
import { DriverModel } from './driver.model';

export class TripModel {
    trip_id: number;
    driver_id: number;
    departure: string;
    src_latitude: number;
    src_longitude: number;
    destination: string;
    dst_latitude: number;
    dst_longitude: number;
    requested_at: Date;

    reviewed_at?: Date;
    reviewer_id?: number;
    approved?: boolean;
    reject_reason?: string;

    finished?: boolean;
    finished_at?: Date;
    arrived?: boolean;

    driver?: DriverModel;

    static fromJson(jsonObject): TripModel {
        let {
            trip_id,
            driver_id,
            departure,
            src_longitude,
            src_latitude,
            destination,
            dst_longitude,
            dst_latitude,
            requested_at,
            
            reviewed_at,
            reviewer_id,
            approved,
            reject_reason,

            finished,
            finished_at,
            arrived,

            driver
        } = jsonObject;

        return {
            trip_id,
            driver_id,
            departure,
            src_longitude: Number(src_longitude),
            src_latitude: Number(src_latitude),
            destination,
            dst_longitude: Number(dst_longitude),
            dst_latitude: Number(dst_latitude),
            requested_at: moment(requested_at).toDate(),

            reviewed_at: reviewed_at == null ? null : moment(reviewed_at).toDate(),
            reviewer_id: reviewer_id == null ? null : Number(reviewer_id),
            approved: approved == null ? null : Boolean(approved),
            reject_reason,

            finished: finished == null ? null : Boolean(finished),
            finished_at: finished_at == null ? null : moment(finished_at).toDate(),
            arrived: arrived == null ? null : Boolean(arrived),

            driver: DriverModel.fromJson(driver)
        };
    }

    static fromJsonArray(jsonArray): TripModel[] {
        return _.map(jsonArray, jsonObject => TripModel.fromJson(jsonObject));
    }
}