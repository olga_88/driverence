import * as moment from 'moment';
import * as _ from 'lodash';
import { DrivingStatusModel, StatusTag } from './status.model';

export class DriverModel {
    driver_id: number;
    user_name: string;
    user_id: number;
    imei: string;
    verified: boolean;
    active: boolean;
    status: string;
    first_name: string;
    last_name: string;
    company_id: string;
    email: string;
    phone_number: string;
    created_at: Date;
    
    full_name?: string;
    
    static fromJson(jsonObject): DriverModel {
        if(!jsonObject) return undefined;

        let { driver_id, user_name, user_id, imei, verified, active, status, first_name, last_name, company_id, email, phone_number, created_at} = jsonObject;

        return {
            driver_id: Number(driver_id),
            user_name,
            user_id: Number(user_id),
            imei,
            verified: Boolean(verified),
            active: Boolean(active),
            status,
            first_name,
            last_name,
            company_id,
            email,
            phone_number,
            created_at: moment(created_at).toDate(),

            full_name: `${first_name || ''} ${last_name || ''}`
        };
    }

    static fromJsonArray(jsonArray): DriverModel[] {
        return _.map(jsonArray, jsonObject => DriverModel.fromJson(jsonObject));
    }
}
