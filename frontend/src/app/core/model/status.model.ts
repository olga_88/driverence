import * as moment from 'moment';
import * as _ from 'lodash';

export enum StatusTag {
  Normal = 'normal',
  Stopped = 'stopped',
  ConnectionLost = 'connection_lost',
  WrongWay = 'wrong_way',
  Emergency = 'emergency'
}

export class DrivingStatusModel {
  driver_id: number;
  tag: StatusTag;
  updated_at: Date;

  static fromJson(jsonObject): DrivingStatusModel {
    if(!jsonObject) return null;

    let { driver_id, tag, timestamp } = jsonObject;

    return {
      driver_id: Number(driver_id),
      tag: tag as StatusTag,
      updated_at: moment(timestamp).toDate()
    };
  }

  static fromJsonArray(jsonArray): DrivingStatusModel[] {
    return _.map(jsonArray, jsonObject => DrivingStatusModel.fromJson(jsonObject));
  }
}