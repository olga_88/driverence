import * as moment from 'moment';
import * as _ from 'lodash';

export class MessageModel {
    message_id: number;
    publisher_name: string;
    type: string; // 'log' | 'alert'
    category: string;
    content: string;
    meta_data: any;
    created_at: Date;

    static fromJson(jsonObject): MessageModel {
        let {
            message_id,
            publisher_id,
            publisher_name,
            type,
            category,
            content,
            meta_data,
            created_at
        } = jsonObject;

        return {
            message_id: Number(message_id),
            publisher_name,
            type,
            category,
            content,
            meta_data,
            created_at: moment(created_at).toDate()
        };
    }

    static fromJsonArray(jsonArray): MessageModel[] {
        return _.map(jsonArray, jsonObject => MessageModel.fromJson(jsonObject));
    }
}