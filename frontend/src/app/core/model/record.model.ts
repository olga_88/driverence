import * as moment from 'moment';
import * as _ from 'lodash';
import { DriverModel } from './driver.model';
import { StreamType } from '../live.service';

export class AnalysisReportModel {
    SPT: number;
    SPJ: number;
    OCA: number;
    Energy: number;
    JQ: number;
    LJ: number;
    hJQ: number;
    AVJ: number;
    Stress: number;
    CLStress: number;
    tags: string[];
    
    static fromJson(jsonObject): AnalysisReportModel {
        if(!jsonObject) return undefined;
        
        let { SPT, SPJ, OCA, Energy, JQ, LJ, hJQ, AVJ, Stress, CLStress, tags } = jsonObject;
        return {
            SPT: Number(SPT),
            SPJ: Number(SPJ),
            OCA: Number(OCA),
            Energy: Number(Energy),
            JQ: Number(JQ),
            LJ: Number(LJ),
            hJQ: Number(hJQ),
            AVJ: Number(AVJ),
            Stress: Number(Stress),
            CLStress: Number(CLStress),
            tags
        };
    }

    static fromJsonArray(jsonArray): AnalysisReportModel[] {
        return _.map(jsonArray, jsonObject => AnalysisReportModel.fromJson(jsonObject));
    }
}

export class RecordModel {
    record_id: number;
    driver_id: number;
    
    audio_file_path: string;
    video_file_path: string;

    uploaded_at: Date;
    
    analysis_result: string;
    analysis_report: AnalysisReportModel;

    driver?: DriverModel;

    latest?: boolean;

    static fromJson(jsonObject): RecordModel {
        if(!jsonObject) return undefined;
        
        let {
          record_id, driver_id, 
          audio_file_path, video_file_path,
          uploaded_at,

          analysis_result, analysis_report,

          driver
        } = jsonObject;

        return {
            record_id: Number(record_id),
            driver_id: Number(driver_id),
            audio_file_path, 
            video_file_path,
            uploaded_at: moment(uploaded_at).toDate(),

            analysis_result,
            analysis_report: AnalysisReportModel.fromJson(analysis_report),
            driver: DriverModel.fromJson(driver)
        };
    }

    static fromJsonArray(jsonArray): RecordModel[] {
        return _.map(jsonArray, jsonObject => RecordModel.fromJson(jsonObject));
    }
}
