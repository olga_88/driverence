import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import * as _ from 'lodash';

import { AuthedSocket } from './authed.socket';
// import { DriverModel, LocationModel, MessageModel } from '../model';

import {DriverModel} from '../model/driver.model';
import {LocationModel} from '../model/location.model';
import {MessageModel} from '../model/message.model';
import {TripModel} from '../model/trip.model';
import { RecordModel } from '../model/record.model';

import { HttpClient } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';
import { DrivingStatusModel, StatusTag } from '../model/status.model';

enum PrefetchData {
	Drivers,
	Locations,
	Messages,
	Alerts,
	PendingTrips,
	ActiveTrips,
	Statuses,
	Records
}

@Injectable()
export class SocketService {
	private ADMIN_EVENT_LOCATION = "location"
	private ADMIN_EVENT_DRIVER = "driver";
	private ADMIN_EVENT_MESSAGE = "message";
	private ADMIN_EVENT_TRIP = "trip";
	private ADMIN_EVENT_STATUS = "status";
	private ADMIN_EVENT_RECORD = "record";

	private _drivers: DriverModel[] = [];
	private _locations: LocationModel[] = [];
	private _messages: MessageModel[] = [];
	private _alerts: MessageModel[] = [];
	private _pendingTrips: TripModel[] = [];
	private _activeTrips: TripModel[] = [];
	private _statuses: DrivingStatusModel[] = [];
	private _lastRecords: RecordModel[] = [];
	
	private _driversSubject: BehaviorSubject<DriverModel[]> = new BehaviorSubject(null);
	private _locationsSubject: BehaviorSubject<LocationModel[]> = new BehaviorSubject(null);
	private _messagesSubject: BehaviorSubject<MessageModel[]> = new BehaviorSubject(null);
	private _alertsSubject: BehaviorSubject<MessageModel[]> = new BehaviorSubject(null);
	private _pendingTripsSubject: BehaviorSubject<TripModel[]> = new BehaviorSubject(null);
	private _activeTripsSubject: BehaviorSubject<TripModel[]> = new BehaviorSubject(null);
	private _statusesSubject: BehaviorSubject<DrivingStatusModel[]> = new BehaviorSubject(null);
	private _lastRecordsSubject: BehaviorSubject<RecordModel[]> = new BehaviorSubject(null);

	private _lastLocationSubject: Subject<LocationModel> = new Subject();
	private _lastMessageSubject: Subject<MessageModel> = new Subject();
	private _lastDriverSubject: Subject<DriverModel> = new Subject();
	private _lastStatusSubject: Subject<DrivingStatusModel> = new Subject();
	private _lastRecordSubject: Subject<RecordModel> = new Subject();

	private prefetchedData: PrefetchData[] = [];
	private prefetchDoneSubject: Subject<PrefetchData> = new Subject();
	private prefetchCompletedSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
	
	constructor(private socket: AuthedSocket, private httpClient: HttpClient) {
		console.log("SocketService created");

		this.getDrivers();
		this.subscribeDrivers();

		this.getLocations();
		this.subscribeLocations();

		this.getMessages();
		this.subscribeMessages();

		this.getPendingTrips();
		this.getActiveTrips();
		this.subscribeTrips();

		this.getStatuses();
		this.subscribeStatus();

		this.getLastRecords();
		this.subscribeRecords();

		// Monitor prefetch is done
		this.prefetchDoneSubject.subscribe(part => {
			this.prefetchedData.push(part);

			if(this.prefetchedData.length === 8) {
				console.log('All data has been fetched');
				this.prefetchCompletedSubject.next(true);
			}
		});
	}

	disconnect() {
		this.socket.disconnect();
	}

	get prefetchCompletedObservable(): Observable<boolean> {
		return this.prefetchCompletedSubject.asObservable();
	}

	getDrivers() {
		this.httpClient.get<DriverModel[]>('/admin/drivers')
			.pipe(
				map((res: any[]) => DriverModel.fromJsonArray(res))
			)
			.subscribe(drivers => {
				this._drivers = drivers;
				this._driversSubject.next(drivers);

				this.prefetchDoneSubject.next(PrefetchData.Drivers);
			});
	}

	subscribeDrivers() {
		this.socket
			.fromEvent<DriverModel>(this.ADMIN_EVENT_DRIVER)
			.subscribe(driver => {
				driver = DriverModel.fromJson(driver);

				_.remove(this._drivers, {"driver_id": driver.driver_id});
				
				this._drivers.push(driver);

				this._driversSubject.next(this._drivers);
				this._lastDriverSubject.next(driver);
			});
	}

	getLocations() {
		this.httpClient.get<LocationModel[]>('/admin/locations')
			.pipe(
				map((res: any[]) => LocationModel.fromJsonArray(res))
			)
			.subscribe(locations => {
				this._locations = locations;
				this._locationsSubject.next(locations);
				this.prefetchDoneSubject.next(PrefetchData.Locations);
			});
	}

	subscribeLocations() {
		this.socket
			.fromEvent<LocationModel>(this.ADMIN_EVENT_LOCATION)
			.subscribe(location => {
				location = LocationModel.fromJson(location);
				
				_.remove(this._locations, {"driver_id": location.driver_id});
			
				this._locations.push(location);
	
				this._locationsSubject.next(this._locations);
				this._lastLocationSubject.next(location);
			});
	}

	getMessages() {
		this.httpClient.get<MessageModel[]>('/admin/messages?type=log')
			.pipe(
				map((res: any[]) => MessageModel.fromJsonArray(res))
			)
			.subscribe(messages => {
				this._messages = messages;
				this._messagesSubject.next(messages);

				this.prefetchDoneSubject.next(PrefetchData.Messages);
			});

		this.httpClient.get<MessageModel[]>('/admin/messages?type=alert')
			.pipe(
				map((res: any[]) => MessageModel.fromJsonArray(res))
			)
			.subscribe(messages => {
				this._alerts = messages;
				this._alertsSubject.next(messages);

				this.prefetchDoneSubject.next(PrefetchData.Alerts);
			});
	}

	subscribeMessages() {
		this.socket
			.fromEvent<MessageModel>(this.ADMIN_EVENT_MESSAGE)
			.subscribe(message => {
				message = MessageModel.fromJson(message);

				if(message.type === 'log') {
					this._messages.unshift(message);
					this._messagesSubject.next(this._messages);
				} else if(message.type === 'alert') {
					this._alerts.unshift(message);
					this._alertsSubject.next(this._alerts);
				}

				this._lastMessageSubject.next(message);
			});
	}

	
	getPendingTrips() {
		this.httpClient.get<TripModel[]>('/admin/trips/pending')
			.pipe(
				map((res: any[]) => TripModel.fromJsonArray(res))
			)
			.subscribe(trips => {
				this._pendingTrips = trips;
				this._pendingTripsSubject.next(trips);

				this.prefetchDoneSubject.next(PrefetchData.PendingTrips);
			});
	}

	getActiveTrips() {
		this.httpClient.get<TripModel[]>('/admin/trips/active')
			.pipe(
				map((res: any[]) => TripModel.fromJsonArray(res))
			)
			.subscribe(trips => {
				this._activeTrips = trips;
				this._activeTripsSubject.next(trips);

				this.prefetchDoneSubject.next(PrefetchData.ActiveTrips);
			});
	}

	subscribeTrips() {
		this.socket
			.fromEvent<TripModel>(this.ADMIN_EVENT_TRIP)
			.subscribe(trip => {
				trip = TripModel.fromJson(trip);

				console.log(trip);

				_.remove(this._pendingTrips, { trip_id: trip.trip_id });
				_.remove(this._activeTrips, { trip_id: trip.trip_id });

				if(trip.approved == null) {
					this._pendingTrips.unshift(trip);
				} else if(trip.approved === true && !trip.finished) {
					this._activeTrips.unshift(trip);
				} else if(trip.finished) {
					_.remove(this.statuses, { driver_id: trip.driver_id });
					this._statusesSubject.next(this.statuses);
				}
				
				this._pendingTripsSubject.next(this._pendingTrips);
				this._activeTripsSubject.next(this._activeTrips);
			});
	}

	getStatuses() {
		return this.httpClient.get(`/admin/statuses`)
    .pipe(
      map((res: any[]) => DrivingStatusModel.fromJsonArray(res))
    )
		.subscribe(statuses => {
			this._statuses = statuses;
			this._statusesSubject.next(statuses);

			this.prefetchDoneSubject.next(PrefetchData.Statuses);
		});
	}

	subscribeStatus() {
		this.socket
			.fromEvent<DrivingStatusModel>(this.ADMIN_EVENT_STATUS)
			.subscribe(status => {
				status = DrivingStatusModel.fromJson(status);

				_.remove(this.statuses, { driver_id: status.driver_id });
				this.statuses.push(status);

				this._statusesSubject.next(this.statuses);
				this._lastStatusSubject.next(status);
			});
	}

	getLastRecords() {
		return this.httpClient.get('/admin/records/last')
		.pipe(
			map((res: any[]) => RecordModel.fromJsonArray(res))
		)
		.subscribe(records => {
			this._lastRecords = records;
			this._lastRecordsSubject.next(records);

			this.prefetchDoneSubject.next(PrefetchData.Records);
		});
	}

	subscribeRecords() {
		this.socket
			.fromEvent<RecordModel>(this.ADMIN_EVENT_RECORD)
			.subscribe(record => {
				record = RecordModel.fromJson(record);

				_.remove(this._lastRecords, { driver_id: record.driver_id });
				this._lastRecords.push(record);

				this._lastRecordSubject.next(record);
				this._lastRecordsSubject.next(this.lastRecords);
			});
	}
	
	get messagesObservable(): Observable<MessageModel[]> {
		return this._messagesSubject.asObservable().pipe(filter(messages => messages != null));
	}

	get alertsObservable(): Observable<MessageModel[]> {
		return this._alertsSubject.asObservable().pipe(filter(messages => messages != null));
	}

	get driversObservable(): Observable<DriverModel[]> {
		return this._driversSubject.asObservable().pipe(filter(drivers => drivers != null));
	}

	get locationsObservable(): Observable<LocationModel[]> {
		return this._locationsSubject.asObservable().pipe(filter(locations => locations != null));
	}

	get pendingTripsObservable(): Observable<TripModel[]> {
		return this._pendingTripsSubject.asObservable().pipe(filter(trips => trips != null));
	}
	
	get activeTripsObservable(): Observable<TripModel[]> {
		return this._activeTripsSubject.asObservable().pipe(filter(trips => trips != null));
	}

	get statusesObservable(): Observable<DrivingStatusModel[]> {
		return this._statusesSubject.asObservable().pipe(filter(statuses => statuses != null));
	}

	get lastRecordsObservable(): Observable<RecordModel[]> {
		return this._lastRecordsSubject.asObservable().pipe(filter(records => records != null));
	}

	get lastMessageObservable(): Observable<MessageModel> {
		return this._lastMessageSubject.asObservable();
	}

	get lastLocationObservable(): Observable<LocationModel> {
		return this._lastLocationSubject.asObservable();
	}

	get lastDriverObservable(): Observable<DriverModel> {
		return this._lastDriverSubject.asObservable();
	}

	get lastStatusObservable(): Observable<DrivingStatusModel> {
		return this._lastStatusSubject.asObservable();
	}

	get lastRecordObservable(): Observable<RecordModel> {
		return this._lastRecordSubject.asObservable();
	}

	get drivers(): DriverModel[] {
		return this._drivers;
	}

	get pendingTrips(): TripModel[] {
		return this._pendingTrips;
	}

	get activeTrips(): TripModel[] {
		return this._activeTrips;
	}

	get messages(): MessageModel[] {
		return this._messages;
	}

	get alerts(): MessageModel[] {
		return this._alerts;
	}

	get locations(): LocationModel[] {
		return this._locations;
	}

	get statuses(): DrivingStatusModel[] {
		return this._statuses;
	}

	get lastRecords(): RecordModel[] {
		return this._lastRecords;
	}
}