import { Injectable } from '@angular/core';
import { Socket, SocketIoConfig } from 'ngx-socket-io';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class AuthedSocket extends Socket {
  constructor(private authenticationService: AuthenticationService) {
    super({
      url: environment.wsUrl,
      options: { 
        query: { 
          token: authenticationService.credentials.token 
        },
        reconnection: true
      },
    });
  }
}
