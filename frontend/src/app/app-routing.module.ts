import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from './core/authentication/authentication.guard';

import { Error404Component } from './shared/error404/error404.component';

const routes: Routes = [
  { 
    path: 'admin', 
    canActivate: [AuthenticationGuard],
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  { path: 'driver', loadChildren: () => import('./driver/driver.module').then(m => m.DriverModule)},
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
