const template = 
`
[Unit]
Description=Campavias Admin Server Application
After=network-online.target

[Service]
Restart=on-failure
WorkingDirectory=__DIR__
ExecStart=__NPM__ run production

[Install]
WantedBy=multi-user.target
`;

const path = require('path');
const fs = require('fs');
const { promisify } = require('util');
const exec = promisify(require('child_process').exec);

let start = async () => {
    const workingDir = path.resolve(__dirname)
    console.log('Working directory:', workingDir);
    
    const npm = await exec('type -P npm');
    if(npm.stderr.trim()) {
        console.error('Unable to find `npm`');
        process.exit(1);
    }
    const npmPath = npm.stdout.trim();

    console.log('npm path:', npmPath);

    let content = template;
    content = content.replace('__DIR__', workingDir);
    content = content.replace('__NPM__', npmPath);

    fs.writeFileSync('/lib/systemd/system/campavias.service', content);
    
    // console.log(content);

    
    console.log('\n\nInstall daemon finished. Please run following commands to enable it.');
    console.log('\nsystemctl daemon-reload && systemctl enable campavias');
};

start();