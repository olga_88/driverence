const _ = require('lodash');

const log = require('./services/logger.service')('Server');

const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const cors = require('cors');

const http = require('http');
const https = require('https');

const auth = require('./policies/auth.policy');
const dbService = require('./services/db.service');
const socketService = require('./services/socket');
const webrtcService = require('./services/webrtc.service');
const broadcastService = require('./services/broadcast.service');
const emergencyService = require('./services/emergency.service');

// environment: development, staging, testing, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */
const app = express();


// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser())

// secure your private routes with jwt authentication middleware
app.all('/api/driver/*', (req, res, next) => auth.authToken(req, res, next));
app.all('/api/admin/*', (req, res, next) => auth.authToken(req, res, next));
app.all('/api/admin/*', (req, res, next) => auth.authAdmin(req, res, next));

app.all('*', (req, res, next) => {
  if(environment === 'development') {
    if (req.path.startsWith('/api')) {
      let token = req.token ? _.omit(req.token,  ['iat', 'exp']) : 'anonymous';
      log.debug(`[${req.method}] Api Request from`, token, 
`
---------------------------------------------------------------------------------------------------------------
Endpoint: ${req.path}
${req.method === 'GET' ? 'Query' : 'Body'}: ${req.method === 'GET' ? JSON.stringify(req.query) : _.truncate(JSON.stringify(req.body, null, 2), {length: 320})}
---------------------------------------------------------------------------------------------------------------
`
      );
    }
  }
  next();
});

app.use('/api/driver', require('./routers/driver.router'));
app.use('/api/admin', require('./routers/admin.router'));
app.use('/api/public', require('./routers/public.router'));
app.use('/api/assets', require('./routers/assets.router'));

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

if (environment === 'production') {
  // Point static path to dist
  app.use(express.static(path.join(__dirname, '../frontend/dist')));

  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../frontend/dist/index.html'));
  });
}

var server;

if (environment === 'production') {
  // var pathDir = "../certs";
  // var https_options = {
  //   ca: fs.readFileSync(pathDir + '/cert.ca-bundle'),
  //   key: fs.readFileSync(pathDir + '/cert.key'),
  //   cert: fs.readFileSync(pathDir + '/cert.crt'),
  // };

  // server = https.createServer(https_options, app);

  server = new http.Server(app)
} else {
  server = new http.Server(app)

}

server.listen(Number(process.env.PORT || 3000), '0.0.0.0', async () => {
  if (
    environment !== 'production' &&
    environment !== 'development'
  ) {
    log.error(
      `NODE_ENV is set to ${environment}, but only production and development are valid.`
    );
    process.exit(1);
  }
  
  log.info(`Rest api server has been initialized`);

  await dbService.start();
  emergencyService.start();
  socketService(server).start();
  broadcastService.start();
  webrtcService(server).start();
});

module.exports = app;