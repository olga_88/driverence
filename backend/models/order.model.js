const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvTripOrders";

let Order = database.define(
	"order",
	{
    	order_id: {
            type: Sequelize.INTEGER,
            field: "OrderId",
            primaryKey: true,
            autoIncrement: true
        },
        
		driver_id: {
			type: Sequelize.INTEGER,
			field: "DriverId",
        },

        latitude: {
            type: Sequelize.STRING(25),
            field: "Latitude"
        },

        longitude: {
            type: Sequelize.STRING(25),
            field: "Longitude"
        },

        destination: {
            type: Sequelize.STRING,
            field: "Destination"
        },

		status: {
			type: Sequelize.ENUM,
			values: ['pending', 'accepted', 'rejected', 'canceled'],
			field: "Status",
			defaultValue: 'pending'
		},

		reject_reason: {
			type: Sequelize.STRING,
			field: "RejectReason"
		},

        created_at: {
            type: Sequelize.DATE,
            field: "CreatedAt",
            defaultValue: Sequelize.NOW,
        },

		expired_at: {
			type: Sequelize.DATE,
            field: "ExpiredAt",
		}
	},
	{
        tableName,
        timestamps: false
	}
);

module.exports = Order;