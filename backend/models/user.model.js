const Sequelize = require("sequelize");

const hashService = require('../services/hash.service');

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "Users";

let User = database.define(
	"user",
	{
		user_id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			allowNull: false,
			field: "UserId",
			unique: true
		},
		
		user_name: {
			type: Sequelize.STRING,
			allowNull: false,
			field: "User"
		},

		password: {
			type: Sequelize.STRING,
			allowNull: false,
			field: "Pass",
			set(value) {
				console.log(value);
				this.setDataValue("password", hashService.password(value));
			}
		},

		admin: {
			type: Sequelize.INTEGER,
			field: "Admin"
		},

		email: {
			type: Sequelize.STRING,
			field: "Email"
		},

		phone_number: {
			type: Sequelize.STRING,
			field: "PhoneNumber"
		},

		first_name: {
			type: Sequelize.STRING,
			field: "FirstName"
		},

		last_name: {
			type: Sequelize.STRING,
			field: "LastName"
		}
	},
	{
		tableName,
		timestamps: false,
	}
);

User.lastUserId = async() => {
	return (await User.max("user_id")) || 1000;
};
module.exports = User;
