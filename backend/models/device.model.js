const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "Devices";

let Device = database.define(
	"device",
	{
        device_id: {
            type: Sequelize.INTEGER,
            field: "DeviceId",
            primaryKey: true
        },
		imei: {
			type: Sequelize.STRING(50),
			field: "Device",
        },
        user_id: {
            type: Sequelize.INTEGER,
            field: "UserId"
        },
		model: {
			type: Sequelize.STRING(150),
			field: "Model",
        },
        os: {
            type: Sequelize.STRING(50),
            field: "Os"
        },
        device_name: {
            type: Sequelize.STRING(100),
            field: "DeviceName",
        },
	},
	{
        tableName,
		timestamps: false,
	}
);

Device.lastDeviceId = async() => {
	return (await Device.max("device_id")) || 1000;
};

module.exports = Device;