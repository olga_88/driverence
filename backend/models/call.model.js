const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvCalls";


/** 
 * @param {string} str
 * @returns {number[]}
 */
let string2ids = (str) => {
	const _ = require("lodash");
	return _.split(_.trim(str, '#'), '#').filter(s => s.trim().length > 0).map(s => Number(s));
}

/** 
 * @type {number[]} ids 
 * @returns {string}
 **/
let ids2string = (ids) => {
	const _ = require("lodash");
	return `#${_.join(ids, '#')}#`;
}

let json2string = (json) => {
	if(json instanceof Array) {
		return JSON.stringify(json);
	}
	return json;
}

let string2json = (str) => {
	if(str instanceof Array) {
		return str;
	}

	try {
		return JSON.parse(str);
	} catch (e) {
		return null;
	}
}

let Call = database.define(
	"call",
	{
		call_id: {
			type: Sequelize.INTEGER,
			allowNull: false,
			field: "CallId",
			primaryKey: true,
			autoIncrement: true
		},

		summary: {
			type: Sequelize.STRING,
			allowNull: false,
			field: "Summary",
		},

		is_default: {
			type: Sequelize.BOOLEAN,
			field: "IsDefault"
		},

		driver_ids: {
			// Driver ids: [1,2,4]
			// String looks like "#1#2#4#"
			type: Sequelize.STRING(4096),
			field: "DriverIds",
			get() {
				return string2ids(this.getDataValue("driver_ids"));
			},
			set(value) {
				this.setDataValue("driver_ids", ids2string(value));
			}
		},

		dialogs: {
			type: Sequelize.STRING(2048),
			validate: {
				notEmpty: true
			},
			field: "JsonData",
			get() {
				return string2json(this.getDataValue("dialogs"));
			},
			set(value) {
				this.setDataValue("dialogs", json2string(value));
			}
		},
		
		created_at: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.NOW,
			field: "CreatedAt"
		},
		updated_at: {
			type: Sequelize.DATE,
			field: "UpdatedAt"
		}
	},
	{
		tableName,
		timestamps: false,
		hooks: {
			beforeSave: async (call, options) => {
				call.updated_at = new Date();

				if(call.is_default) {
					// Make other calls no default
					await Call.update({
						is_default: false
					}, {
						where: {
							call_id: { $ne : call.call_id }
						}
					});
				} else {
					// Make this default if there is no default yet
					let numberOfCalls = await Call.count({where: { 
						is_default: true,
						call_id: { $ne: call.call_id }
					}});

					if(numberOfCalls === 0) {
						call.is_default = true;
					}	
				}
			},

			beforeDestroy: async (call, options) => {

				// Make another call default
				if(call.is_default === true) {
					let first = await Call.findOne({
						where: { 
							call_id: {$ne: call.call_id}
						}
					});
					if(first) {
						await first.update({
							is_default: true
						});
					}
				}

			}
		}
	}
);

Call.ids2string = ids2string;
Call.string2ids = string2ids;

Call.json2string = json2string;
Call.string2json = string2json;
module.exports = Call;