//@ts-check
const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvMessages";

let Message = database.define(
	"message",
	{
        message_id: {
            type: Sequelize.INTEGER,
			allowNull: false,
            primaryKey: true,
            autoIncrement: true,
			field: "MessageId",
		},
        publisher_name: {
            type: Sequelize.STRING, // "Username", "Monitor Service", "Registration Service", "Analayze Service"
            allowNull: false,
            field: "PublisherName"
        },
		type: {
			type: Sequelize.ENUM,
			values: ['log', 'alert', 'report'],
            field: "Type",
            defaultValue: 'log'
        },
        content: {
            type: Sequelize.STRING(1024),
            field: "Content"
		},
		category: {
			type: Sequelize.STRING(32),
			field: "Category", 
			// ["sos", "risk"]
			// ["driver_register", "driving_request", "approved", "rejected"]
			// ["bug"]
		},
        meta_data: { // JSON
            type: Sequelize.STRING(2048),
            field: "MetaData",
			validate: {
				notEmpty: true
			},
			set(value) {
				let strValue = value instanceof String ? value : JSON.stringify(value);
				this.setDataValue("meta_data", strValue);
			},
			get() {
				let strValue = this.getDataValue("meta_data");
				return JSON.parse(strValue);
			}
        },
		created_at: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.NOW,
			field: "CreatedAt"
		},
	},
	{
        tableName,
		timestamps: false
	}
);

module.exports = Message;