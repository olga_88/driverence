const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvResetPwdToken";

let ResetPwdToken = database.define(
	"token",
	{
		token_id: {
			type: Sequelize.INTEGER,
      field: "TokenId",
      primaryKey: true,
      autoIncrement: true
		},

    email: {
      type: Sequelize.STRING,
      field: "Email"
    },

    token: {
      type: Sequelize.STRING,
      field: "Token"
    },

		created_at: {
			type: Sequelize.DATE,
			field: "CreatedAt",
      defaultValue: Sequelize.NOW,
		}
	},
	{
		tableName,
    timestamps: false
	}
);

module.exports = ResetPwdToken;