const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvApps";

let App = database.define(
	"app",
	{
		app_id: {
			type: Sequelize.INTEGER,
      field: "AppId",
      primaryKey: true,
      autoIncrement: true
		},

    summary: {
      type: Sequelize.STRING,
      field: "Summary"
    },

		version: {
				type: Sequelize.STRING(25),
				field: "Version"
		},

		file_path: {
				type: Sequelize.STRING,
				field: "FilePath"
		},

		uploaded_at: {
			type: Sequelize.DATE,
			field: "UploadedAt",
      defaultValue: Sequelize.NOW,
		},
	},
	{
		tableName,
    timestamps: false
	}
);

module.exports = App;