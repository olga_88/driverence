const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvTrips";

let Trip = database.define(
	"trip",
	{
        trip_id: {
            type: Sequelize.INTEGER,
            field: "TripId",
            primaryKey: true,
            autoIncrement: true
        },
        
		driver_id: {
			type: Sequelize.INTEGER,
			field: "DriverId",
        },

        departure: {
            type: Sequelize.STRING,
            field: "Departure"
        },

        src_latitude: {
            type: Sequelize.STRING(25),
            field: "SrcLatitude"
        },

        src_longitude: {
            type: Sequelize.STRING(25),
            field: "SrcLongitude"
        },

        destination: {
            type: Sequelize.STRING,
            field: "Destination"
        },

        dst_latitude: {
            type: Sequelize.STRING(25),
            field: "DstLatitude"
        },

        dst_longitude: {
            type: Sequelize.STRING(25),
            field: "DstLongitude"
        },

        requested_at: {
            type: Sequelize.DATE,
            field: "RequestedAt",
            defaultValue: Sequelize.NOW,
        },

        reviewed_at: {
            type: Sequelize.DATE,
            field: "ReviewedAt"
        },

        reviewer_id: {
            type: Sequelize.INTEGER,
            field: "ReviewerId"
        },

        approved: {
            type: Sequelize.BOOLEAN,
            field: 'Approved'
        },

        reject_reason: {
            type: Sequelize.STRING(1024),
            field: 'RejectReason'
        },

        finished: {
            type: Sequelize.BOOLEAN,
            field: "Finished"
        },

        finished_at: {
            type: Sequelize.DATE,
            field: "FinishedAt"
        },

        arrived: {
            type: Sequelize.BOOLEAN,
            field: "Arrived"
            // FALSE case: Canceled, Rejected
        }
	},
	{
        tableName,
        timestamps: false
	}
);

module.exports = Trip;