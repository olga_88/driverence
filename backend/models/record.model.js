const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');
const { sequelize } = require("./user.model");


let json2string = (json) => {
	if(json instanceof Object) {
		return JSON.stringify(json);
	}
	return json;
}

let string2json = (str) => {
	if(str instanceof Object) {
		return str;
	}

	try {
		return JSON.parse(str);
	} catch (e) {
		return null;
	}
}

const tableName = "DrvRecords";

let Record = database.define(
	"record",
	{
		record_id: {
				type: Sequelize.INTEGER,
				field: "RecordId",
				primaryKey: true,
				autoIncrement: true
		},
		
		driver_id: {
			type: Sequelize.INTEGER,
			field: "DriverId",
		},

		audio_file_path: {
				type: Sequelize.STRING,
				field: "AudioFilePath"
		},

		video_file_path: {
				type: Sequelize.STRING,
				field: "VideoFilePath"
		},

		uploaded_at: {
			type: Sequelize.DATE,
			field: "UploadedAt",
			defaultValue: Sequelize.NOW,
		},

		analysis_result: {
			type: Sequelize.STRING,
			field: "AnalysisResult"
		},

		analysis_report: {
			type: Sequelize.STRING,
			field: "AnalysisReport",
			get() {
				return string2json(this.getDataValue("analysis_report"));
			},
			set(value) {
				this.setDataValue("analysis_report", json2string(value));
			}
		}
	},
	{
        tableName,
        timestamps: false
	}
);

Record.lastIds = async () => {
	return Record.findAll({
		attributes: [
			[Sequelize.col('DriverId'), 'driver_id'],
			[Sequelize.fn("max", Sequelize.col('RecordId')), 'record_id']
		],
		group: [Sequelize.col('DriverId')],
	});
};
module.exports = Record;