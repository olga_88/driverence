const Sequelize = require("sequelize");
const hashService = require('../services/hash.service');

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "Drivers";

let Driver = database.define(
	"driver",
	{
		driver_id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			field: "DriverId",
			autoIncrement: true
		},

		company_id: {
			type: Sequelize.STRING,
			field: "CompanyId",
		},

		user_name: {
			type: Sequelize.STRING,
			field: "UserName"
		},

		password: {
			type: Sequelize.STRING,
			allowNull: false,
			field: "Password",
			set(value) {
				this.setDataValue("password", hashService.password(value));
			}
		},

		imei: {
			type: Sequelize.STRING,
			allowNull: true,
			field: "IMEI",
		},

		verified: {
			type: Sequelize.BOOLEAN,
			defaultValue: false,
			field: "Verified"
		},

		active: {
			type: Sequelize.BOOLEAN,
			defaultValue: true,
			field: "Active"
		},

		status: {
			type: Sequelize.ENUM,
			values: ['normal', 'waiting_approval', 'on_trip'],
			field: "Status",
			defaultValue: 'normal'
		},
		
		user_id: {
			type: Sequelize.INTEGER,
			allowNull: false,
			field: "UserId"
		},

		email: {
			type: Sequelize.STRING,
			field: "Email"
		},

		phone_number: {
			type: Sequelize.STRING,
			field: "PhoneNumber"
		},

		first_name: {
			type: Sequelize.STRING,
			field: "FirstName"
		},

		last_name: {
			type: Sequelize.STRING,
			field: "LastName"
		},

		push_token: {
			type: Sequelize.STRING,
			field: "PushToken"
		},

		push_type: {
			type: Sequelize.ENUM,
			values: ['fcm', 'apn'],
			defaultValue: "fcm",
			field: "PushType"
		},

		created_at: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.NOW,
			field: "CreatedAt"
		},

		full_name: {
			type: Sequelize.VIRTUAL,
			get() {
				return `${this.first_name} ${this.last_name}`
			}
		}
	},
	{
		tableName,
		timestamps: false
	}
);

module.exports = Driver;