module.exports = {
	User: require('./user.model'),
	Driver: require('./driver.model'),
	Call: require('./call.model'),
	Message: require('./message.model'),
	Device: require('./device.model'),
	Trip: require('./trip.model'),
	Location: require('./location.model'),
	Record: require('./record.model'),
	App: require('./app.model'),
	ResetPwdToken: require('./token.model'),
	Order: require('./order.model')
};
