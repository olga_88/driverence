const Sequelize = require("sequelize");

/** @type {Sequelize.Sequelize} */
const database = require('../database');

const tableName = "DrvLocations";

let Location = database.define(
	"location",
	{
		driver_id: {
			type: Sequelize.INTEGER,
            field: "DriverId",
            primaryKey: true
		},

		latitude: {
				type: Sequelize.STRING(25),
				field: "Latitude"
		},

		longitude: {
				type: Sequelize.STRING(25),
				field: "Longitude"
		},

		updated_at: {
			type: Sequelize.DATE,
			field: "UpdatedAt"
		},
	},
	{
		tableName,
        timestamps: false,
        hooks: {
			beforeSave: (location, options) => {
				location.updated_at = new Date();
			}
		}
	}
);

module.exports = Location;