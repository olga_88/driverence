const authService = require('../services/auth.service');

authToken = (req, res, next) => {
	let tokenToVerify;

  if (req.header('Authorization')) {
    const parts = req.header('Authorization').split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/.test(scheme)) {
        tokenToVerify = credentials;
      } else {
        return res.status(401).json({ msg: 'Format for Authorization: Bearer [token]' });
      }
    } else {
      return res.status(401).json({ msg: 'Format for Authorization: Bearer [token]' });
    }
  } else if (req.body.token) {
    tokenToVerify = req.body.token;
    delete req.query.token;
  } else {
    return res.status(401).json({ msg: 'Authorization not found' });
  }

  return authService.verify(tokenToVerify, (err, thisToken) => {
    if (err) return res.status(401).json({ err });
    req.token = thisToken;
    return next();
  });
}

authAdmin = (req, res, next) => {
	if (req.token && req.token.role === "admin") {
		return next();
	} else {
		return res.status(403).json({ msg: 'Forbidden by admin priviliege'});
	}
}

module.exports = {
	authToken,
	authAdmin
};