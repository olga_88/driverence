//@ts-check
const express = require('express');

module.exports = {
    /**
     * @param {express.Response} res 
     * @param {string} msg
     * 
    */
    param_error: (res, msg) => {
        res.status(400).json({msg: `${msg}`});
    },

    /** 
     * @param {express.Response} res 
     * @param {string} msg
     **/
    auth_error: (res, msg) => {
        res.status(401).json({msg: `Authorization failed. ${msg}`});
    },

    /** 
     * @param {express.Response} res 
     **/
    server_error: (res) => {
        res.status(500).json({msg: 'Internal server error.'});
    }

}