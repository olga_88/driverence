const _ = require('lodash');
const log = require('../services/logger.service')('PublicRouter');

const Model = require("../models");

const registerService = require('../services/register.service');
const hashService = require('../services/hash.service');
const authService = require('../services/auth.service');

const responseUtil = require('./response.util');

var router = require('express').Router();
// Purpose for temporary usuage;
router.post('/registerAdmin', async (req, res) => {
	if(process.env.NODE_ENV === "production") {
		log.warn("Register admin in production mode is prohibited");
		return res.status(404);
	}

	const { user_name, email, password} = req.body;
	try {
		let user = user_name ? await Model.User.findOne({
			where: { user_name }
		}) : null;

		if(!user) {
			user = await Model.User.create({
				user_id: await Model.User.lastUserId() + 1,
				user_name,
				password,
				email,
				admin: 1
			});
			return res.json(user);
		} else {
			return responseUtil.param_error(res, `User name ${user_name} is already taken`);
		}
	} catch (err) {
		log.error(`Failed to register new admin`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/loginAdmin', async (req, res) => {
	const { email, password } = req.body;

	if(email == null || password == null) {
		return responseUtil.param_error(res, `"email" and "password" fields are required.`);
	}

	try {
		let user = await Model.User.findOne({where: {
			$or: [
				{user_name: email},
				{email: email},
			]
		}});
	
		if(user) {
			if(hashService.comparePassword(password, user.password)) {
				if(user.admin === 1) {
					const token = authService.issue({user_id: user.user_id, role: "admin" });
					return res.json({ token, user: _.omit(user.toJSON(), "password") });
				} else {
					return responseUtil.auth_error(res, "Forbidden privillege");
				}
			}
		}
		
		return responseUtil.auth_error(res, "Username or password is incorrect");

	}	catch (err) {
		log.error(`Failed to login as admin, ${email}`, err);
		return responseUtil.server_error(res);
	}

});

router.post('/registerDriver', async (req, res) => {
	// token is a Reset Password Token
	const { email, password, code, imei } = req.body;
	
	if(_.isEmpty(email)) return responseUtil.param_error(res, "Email is empty.");
	if(_.isEmpty(password)) return responseUtil.param_error(res, "Password is empty.");
	if(_.isEmpty(code)) return responseUtil.param_error(res, "Registeration code is empty.");
	if(_.isEmpty(imei)) return responseUtil.param_error(res, "Device Id is empty.");

	try {
		let driver = await Model.Driver.findOne({
			where: {
				email
			}
		});

		if(!driver) {
			return responseUtil.param_error(res, "Driver does not exist.");
		}

		let rpt = await Model.ResetPwdToken.findOne({
			where: {
				email, token: code
			}
		});

		if(!rpt) {
			return responseUtil.param_error(res, "Invalid code.");
		}

		await driver.update({
			verified: true,
			password,
			imei
		});

		device = await Model.Device.findOne({
			where: {
				imei
			}
		});

		if(device) {
			// return responseUtil.param_error(res, `Device is already registered by another user`);
			await device.update({
				user_id: driver.user_id
			});
		} else {
			device = await Model.Device.create({
				device_id: await Model.Device.lastDeviceId() + 1,
				user_id: driver.user_id,
				imei
			});
	
			if(!device) {
				throw new Error('Failed to create device model.');
			}
		}

		log.db({
			publisher_name: "Registration Service",
			type: 'log',
			content: `${driver.first_name || ''} ${driver.last_name || ''} has been joined driver's program`,
			category: 'driver_register'
		});
	
		const token = authService.issue({
			driver_id: driver.driver_id, 
			user_id: driver.user_id, 
			role: "driver" });

		return res.json({
			driver: _.omit(driver.toJSON(), "password"), 
			// user: _.omit(user.toJSON(), "password"),
			token});
	} catch (err) {
		log.error('Failed to register driver', err);

		return responseUtil.server_error(res);
	}
})

router.post('/loginDriver', async(req, res) => {
	const { email, password } = req.body;

	if(email == null || password == null) return res.status(400).json({ msg: "Invalid parameter: Email and password is required"});

	try {
		let driver = await Model.Driver.findOne({ where: {
			email
		}});

		if(!driver) return responseUtil.param_error(res, `Driver ${email} does not exist`);
		else if(!hashService.comparePassword(password, driver.password)) return res.status(401).json({ msg: "Password does not match"});

		const token = authService.issue({
			driver_id: driver.driver_id, 
			user_id: driver.user_id, 
			role: "driver" });

		return res.status(200).json({
			driver: _.omit(driver.toJSON(), "password"), 
			// user: _.omit(user.toJSON(), "password"),
			token});
	} catch (err) {
		log.error(`Failed to login as driver, ${user_name}`, err);

		return responseUtil.server_error(res);
	}
});

module.exports = router;