const _ = require('lodash');
const log = require('../services/logger.service')('AssetsRouter');
const path = require('path');

const Model = require('../models');

let authService = require('../services/auth.service');

var router = require('express').Router();

const call_dir = path.join(global.appRoot, '/uploads/calls');
const record_dir = path.join(global.appRoot, '/uploads/records');
const app_dir = path.join(global.appRoot, '/uploads/apps');

router.get('/uploads/calls/:filename', async (req, res) => {
    let {token} = req.query;
    let {filename} = req.params;
    authService.verify(token, (err, thisToken) => {
        if (err) return res.status(401).json({ err });
        
        
        return res.sendFile(path.join(call_dir, filename));
    });
});

router.get('/uploads/records/:filename', async(req, res) => {
    let {token} = req.query;
    let {filename} = req.params;
    authService.verify(token, (err, thisToken) => {
        if (err) return res.status(401).json({ err });
        
        let {role} = thisToken;

        if(role === 'admin') {
            return res.sendFile(path.join(record_dir, filename));
        }
    });
});

router.get('/uploads/apps/:filename', async(req, res) => {
    let {token} = req.query;
    let {filename} = req.params;
    authService.verify(token, (err, thisToken) => {
        if (err) return res.status(401).json({ err });
        let {role} = thisToken;
        if(role === 'admin') {
            return res.sendFile(path.join(app_dir, filename));
        }
    });
});

router.get('/download/apps/latest', async(req, res) => {
    let {ref} = req.query;

    let token = await Model.ResetPwdToken.findOne({
        where: {
            token: ref
        }
    });

    if(!token) {
        return res.status(401).send('Invalid reference');
    }

    let app = await Model.App.findOne({
        order: [
            ['app_id', 'DESC']
        ]
    });

    if(app) {
        return res.sendFile(path.join(global.appRoot, app.file_path));
    } else {
        return res.status(404).send('Sorry, something unexpected is happend');
    }
});
module.exports = router;