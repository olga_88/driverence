const _ = require('lodash');
const log = require('../services/logger.service')('AdminRouter');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

const tripService = require('../services/trip.service');
const liveService = require('../services/live.service');
const registerService = require('../services/register.service');

const multer = require('multer');
const uploadCalls = multer({
	dest: 'uploads/calls'
});

const uploadApps = multer({
	dest: 'uploads/apps'
});

const Model = require('../models');
const responseUtil = require('./response.util');
const emergencyService = require('../services/emergency.service');
const { sequelize } = require('../models/user.model');
const { isUndefined } = require('lodash');

var router = require('express').Router();

router.get('/users/:user_id', async(req, res) => {
	try {
		let { user_id } = req.params;
		let user = await Model.User.findByPk(user_id, {
			attributes: ['user_name', 'password']
		});

		return res.json(user);
	} catch (err) {
		log.error(`Failed to get user ${user_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/calls', async (req, res) => {
	try {
		let calls = await Model.Call.findAll({raw: true});

		calls = _.map(calls, call => {
			try {
				let dialogs = Model.Call.string2json(call['dialogs']);
				let driver_ids = Model.Call.string2ids(call['driver_ids']);

				call['dialogs'] = dialogs;
				call['driver_ids'] = driver_ids;
				return call;
			} catch (err) {
				log.error(`Faild to parse dialog`, err);
				return null;
			}
		}).filter(call => call != null);
	
		return res.json(calls);
	} catch (err) {
		log.error(`Failed to get call list`, err);
		return responseUtil.server_error(res);
	}
})

router.post('/calls', uploadCalls.any(), async (req, res) => {
	let files = req.files;
	let { summary, dialogs } = req.body;

	if(!summary || !dialogs) {
		return responseUtil.param_error(res, 'Missing summary or dialogs');
	}

	dialogs = JSON.parse(dialogs);

	let integrity = true;
	_.forEach(dialogs, message => {
		let {fieldname} = message;

		let file = _.find(files, {fieldname});
		if(!file) {
			integrity = false;
			log.warn(`File ${fieldname} is not found`);
			return;
		} 

		message.fieldname = undefined;
		message.file_path = `${file['destination']}/${file['filename']}`;
		message.mimetype = file['mimetype'];
	});
	
	try {
		let call = await Model.Call.create({
			summary,
			dialogs
		});
		
		return res.json(call);
	} catch (err) {
		log.error(`Failed to create call`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/calls/duration', async(req, res) => {
	let { duration } = req.body;

	duration = tripService.setCallDuration(Number(duration));
	return res.json({duration});
});

router.get('/calls/duration', async(req, res) => {
	let duration = Number(tripService.getCallDuration());
	return res.json({duration});
});

router.delete('/calls/:call_id', async(req, res) => {
	let { call_id } = req.params;

	try {
		let call = await Model.Call.findByPk(call_id);

		if(!call) {
			return responseUtil.param_error(`Call #${call_id} is not found`);
		}

		// Delete call audio files
		let files = _.map(call.dialogs, d => path.join(global.appRoot, d.file_path));
		log.debug('remove files', files);
		_.forEach(files, f => {
			try { fs.rmSync(f) } catch (ignored) {}
		});
	
		await call.destroy();
		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to delete call #${call_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.put('/calls/:call_id', async (req, res) => {
	let { call_id } = req.params;

	let { driver_ids, is_default } = req.body;

	try {
		let call = await Model.Call.findByPk(call_id);

		// Remove new ids from other calls
		// let new_ids = _.pullAll(driver_ids, call.driver_ids);
		// if(new_ids.length > 0) {
		// 	_.forEach(await Model.Call.findAll({
		// 		where: {
		// 			driver_ids: {
		// 				$regexp: _.map(new_ids, id => `#${id}#`).join('|')
		// 			},
		// 			call_id: {
		// 				$ne: call_id
		// 			}
		// 		}
		// 	}), async (c) => {
		// 		await c.update({
		// 			driver_ids: _.pullAll(c.driver_ids, new_ids)
		// 		});
		// 	});
		// }

		await call.update({
			is_default,
			driver_ids
		});

		return res.json(call);
	} catch (err) {
		log.error(`Failed to update call #${call_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/drivers/register', async (req, res) => {
	let { user_name, first_name, last_name, email, phone_number, company_id } = req.body;

	if(_.isEmpty(user_name)) {
		return responseUtil.param_error(res, `User name is empty`);
	} else if(_.isEmpty(first_name)) {
		return responseUtil.param_error(res, `First name is empty`);
	} else if(_.isEmpty(last_name)) {
		return responseUtil.param_error(res, `Last name is empty`);
	} else if(_.isEmpty(email)) {
		return responseUtil.param_error(res, `Email is empty`);
	} else if(_.isEmpty(phone_number)) {
		return responseUtil.param_error(res, `Phone number is empty`);
	} else if(_.isEmpty(company_id)) {
		return responseUtil.param_error(res, `Company ID is empty`);
	}

	try {
		
		let { driver, error } = await registerService.registerDriver({
			user_name, first_name, last_name, email, phone_number, company_id, 
			preregister : true
		});

		if(error) {
			return responseUtil.param_error(res, error);
		}

		return res.json(_.omit(driver.toJSON(), "password"));
	} catch (err) {
		log.error(`Failed to register driver`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/drivers/:driver_id/invite', async (req, res) => {
	let { driver_id } = req.params;

	try {
		let driver = await Model.Driver.findByPk(driver_id);
		if(!driver) {
			return responseUtil.param_error(res, `Driver ${driver_id} does not exist`);
		}

		await registerService.sendInvitation(driver.email);

		return res.json({success: true});
	} catch (err) {
		log.error(err);
		return responseUtil.server_error(res);
	}
});

router.get('/drivers', async (req, res) => {
	const { active } = req.body;
	try {
		let drivers = await Model.Driver.findAll({
			attributes: {
				exclude: ['password']
			},
			raw: true
		});
		return res.json(drivers);
	} catch (err) {
		log.error(`Failed to get driver list`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/messages', async(req, res) => {
	let {type} = req.query;
	if(!type) type = 'log';

	if(type !== 'log' && type !== 'alert') {
		return responseUtil.param_error(res, `Unknown type ${type}`);
	}

	try {
		let messages = await Model.Message.findAll({
			where: {type},
			order: [
				['created_at', 'DESC']
			],
			limit: 1000
		});
	
		return res.json(messages);
	} catch (err) {
		log.error(`Failed to get message list`, err);
		return responseUtil.server_error(res);
	}
});

router.delete('/messages/:message_id', async(req, res) => {
	let { message_id } = req.params;

	try {
		await Model.Message.destroy({
			where: {
				message_id
			}
		});

		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to delete messages`, err);
		return responseUtil.server_error(res);
	}
});

router.delete('/messages', async(req, res) => {
	let { ids } = req.body;
	try {
		await Model.Message.destroy({
			where: {
				message_id: {
					$in: ids
				}
			}
		});

		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to delete messages`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/records', async(req, res) => {
	try {
		let records = await Model.Record.findAll({
			include: [
				{
					model: Model.Driver, 
					as: 'driver', 
					attributes: ['driver_id', 'user_name', 'first_name', 'last_name']
				}
			],
			order: [
				['uploaded_at', 'DESC']
			]
		});
		res.json(records);
	} catch (err) {
		log.error(`Failed to get record list`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/records/last', async(req, res) => {
	try {
		let lastIds = await Model.Record.lastIds();

		let records = await Model.Record.findAll({
			where: {
				record_id: { $in: lastIds.map(l => l.record_id) }
			},
			// include: [
			// 	{
			// 		model: Model.Driver, 
			// 		as: 'driver', 
			// 		attributes: ['driver_id', 'user_name', 'first_name', 'last_name']
			// 	}
			// ],
		});
		res.json(records);
	} catch (err) {
		log.error(`Failed to get record list`, err);
		return responseUtil.server_error(res);
	}
});

router.delete('/records/:record_id', async(req, res) => {
	const { record_id } = req.params;
	
	try {
		let record = await Model.Record.findByPk(record_id);

		if(!record) {
			throw new Error(`Can not find Record #${record_id}`);
		}

		// delete record files
		let files = [];
		files.push(path.join(global.appRoot, record.audio_file_path));
		files.push(path.join(global.appRoot, record.video_file_path));
		log.debug('remove files', files);
		_.forEach(files, f => {
			try { fs.rmSync(f) } catch (ignored) {}
		});

		await record.destroy();
		return res.json({ success: true });
	} catch (err) {
		log.error(`Failed to delete Record #${record_id}`, err);
		return responseUtil.server_error(res);
	}
});


router.get('/trips', async(req, res) => {
	try {
		let trips = await Model.Trip.findAll({
			include: [
				{
					model: Model.Driver, 
					as: 'driver', 
					attributes: ['driver_id', 'user_name', 'first_name', 'last_name']
				}
			],
			order: [
				['requested_at', 'DESC']
			]
		});
		res.json(trips);
	} catch (err) {
		log.error(`Failed to get trip list`, err);
		return responseUtil.server_error(res);
	}
});

router.put('/trips/:trip_id/approval', async(req, res) => {
	let { user_id } = req.token;
	const { trip_id } = req.params;
	let {approved, reject_reason} = req.body;

	if(approved == null) {
		return responseUtil.param_error(res, `"approved" field is empty.`);
	}

	if(trip_id == null) {
		return responseUtil.param_error(res, `"trip_id" field is empty.`);
	}

	if(approved == false && reject_reason == null) {
		return responseUtil.param_error(res, `"reject_reason" field is empty.`);
	}

	try {
		if(approved) {
			await tripService.approve(trip_id, user_id);
		} else {
			await tripService.reject(trip_id, reject_reason, user_id);
		}

		return res.json({sucess: true});
	} catch (err) {
		log.error(`Failed to process approve/reject ${trip_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.delete('/trips/:trip_id', async(req, res) => {
	let { trip_id } = req.params;

	try {
		let trip = await Model.Trip.findByPk(trip_id);

		if(!trip) {
			return responseUtil.param_error(res, `Trip #${trip_id} is not found`);
		}

		await trip.destroy();

		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to delete trip #${trip_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/trips/pending', async (req, res) => {

	try {
		let trips = await Model.Trip.findAll({
			where:  {
				approved: null
		}});

		return res.json(trips);
	} catch (err) {
		log.error(`Failed to get trips`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/trips/active', async (req, res) => {
	try {
		let trips = await Model.Trip.findAll({
			include: [
				{
					model: Model.Driver, 
					as: 'driver', 
					attributes: ['driver_id', 'user_name', 'first_name', 'last_name']
				}
			],
			order: [
				['requested_at', 'DESC']
			],
			where:  {
				approved: true,
				$or: [
					{ finished: null },
					{ finished: false}
				]
		}});

		return res.json(trips);
	} catch (err) {
		log.error(`Failed to get trips`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/trips/finished', async (req, res) => {
	try {
		let trips = await Model.Trip.findAll({
			include: [
				{
					model: Model.Driver, 
					as: 'driver', 
					attributes: ['driver_id', 'user_name', 'first_name', 'last_name']
				}
			],
			order: [
				['requested_at', 'DESC']
			],
			where:  {
				approved: true,
				finished: true
		}});

		return res.json(trips);
	} catch (err) {
		log.error(`Failed to get trips`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/locations', async (req, res) => {
	try {
		let locations = await Model.Location.findAll();
		return res.json(locations);
	} catch (err) {
		log.error(`Failed to get locations`, err);
		return responseUtil.server_error(res);
	}
});


router.post('/live/start', async (req, res) => {
	let { driver_id, stream_type } = req.body;

	if(driver_id == null) {
		return responseUtil.param_error(res, 'Driver id is empty');
	}

	if(stream_type == null) {
		return responseUtil.param_error(res, 'Stream type is not specified');
	}

	let room_id = await liveService.startLiveStream(driver_id, stream_type);
	return res.json({room_id});
});

router.post('/live/stop', async (req, res) => {
	return res.json({success: true});
});

router.get('/statuses', async (req, res) => {
	return res.json(emergencyService.statuses);
});

router.post('/apps/upload', uploadApps.single('file'), async (req, res) => {
	let file = req.file;

	let { version, summary } = req.body;

	try {
		if(!file) {
			return responseUtil.param_error(res, 'File is not uploaded.');
		}

		if(_.isEmpty(version)) {
			return responseUtil.param_error(res, 'App version is not specified.');
		}

		let file_path = `${file.destination}/${file.filename}`;

		let app = await Model.App.create({
			summary,
			version,
			file_path
		});

		return res.json(app);
	} catch (err) {
		log.error(`Failed upload app file`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/apps', async (req, res) => {
	try {
		let apps = await Model.App.findAll({
			order: [
				['app_id', 'DESC']
			]
		});

		return res.json(apps);
	} catch (err) {
		log.error(`Can not get app list`, err);
		return responseUtil.server_error(res);
	}
});

router.delete('/apps/:app_id', async (req, res) => {
	let { app_id } = req.params;

	try {
		let app = await Model.App.findByPk(app_id);

		if(!app) {
			return responseUtil.param_error(res, `App #${app_id} is not found`);
		}

		// Delete call audio files
		let file = path.join(global.appRoot, app.file_path);
		log.debug('remove app file', file);
		try { fs.rmSync(file) } catch (ignored) {}
	
		await app.destroy();
		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to delete app #${app_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/orders', async (req, res) => {
	let {
		driver_id, longitude, latitude, destination
	} = req.body;

	if(driver_id == null) {
		return responseUtil.param_error(res, `Driver Id is empty`);
	} else if(latitude == null) {
		return responseUtil.param_error(res, `Latitude is empty`);
	} else if(longitude == null) {
		return responseUtil.param_error(res, `Longitude is empty`);
	} else if(destination == null) {
		return responseUtil.param_error(res, `Destination is empty`);
	}

	try {
		let existing = await Model.Order.findOne({
			where: {
				driver_id,
				status: 'pending'
			}
		});

		if(existing) {
			return responseUtil.param_error(res, `There is a pending order already`);
		}

		let order = await Model.Order.create({
			driver_id, latitude, longitude, destination
		});

		if(!order) {
			throw new Error(`Failed to create order`);
		}

		tripService.sendOrder(order);

		return res.json(order);
	} catch (err) {
		log.error(`Failed to create new trip order`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/orders', async (req, res) => {
	try {
		let orders = await Model.Order.findAll({
			order: [
				['created_at', 'DESC']
			],
			limit: 1000
		});
		return res.json(orders);
	} catch (err) {
		log.error(`Failed to get orders`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/orders/:order_id/cancel', async (req, res) => {
	let { order_id } = req.params;
	try {
		let order = await Model.Order.findByPk(order_id);

		if(!order) {
			return responseUtil.param_error(res, `Order #${order_id} is not found`);
		}

		if(order.status !== 'pending') {
			return responseUtil.param_error(res, `Order #${order_id} is not pending now.`);
		}

		await order.update({
			status: 'canceled',
			expired_at: new Date()
		});

		return res.json(order);
	} catch (err) {
		log.error(`Failed to cancel order`, err);
		return responseUtil.server_error(res);
	}
});

router.delete('/orders/:order_id', async (req, res) => {
	let { order_id } = req.params;

	try {
		let order = await Model.Order.findByPk(order_id);
		if(!order) {
			return responseUtil.param_error(res, `Order #${order_id} is not found`);
		}

		await order.destroy();

		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to delete order`, err);
		return responseUtil.server_error(res);
	}
});

module.exports = router;