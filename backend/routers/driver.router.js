const _ = require('lodash');
const log = require('../services/logger.service')('DriverRouter');
const multer = require('multer');

const Model = require('../models');
const tripService = require('../services/trip.service');
const emergencyService = require('../services/emergency.service');

const responseUtil = require('./response.util');

const upload = multer({
	dest: 'uploads/records'
});

var router = require('express').Router();
router.put('/device', async(req, res) => {
	let {user_id} = req.token;
	let {imei, model, os, device_name} = req.body;

	if(imei == null) {
		return responseUtil.param_error(res, `"imei" is not specified`);
	}

	try {
		await Model.Device.update({
			imei,
			model,
			os,
			device_name
		}, { where: { user_id }});
	
		return res.json({sucess: true});
	} catch (err) {
		log.error(`Failed to update device info`, err);
		return responseUtil.server_error(res);
	}
});

router.put('/pushToken', async (req, res) => {
	let {push_token, push_type} = req.body;
	let { driver_id} = req.token;

	if(push_token == null || push_type == null) {
		return responseUtil.param_error(res, 'Either "push_token" or "push_type" is not specified');
	}

	try {
		await Model.Driver.update({
			push_token,
			push_type
		}, { where: {driver_id}});
	
		return res.json({sucess: true});
	} catch (err) {
		log.error(`Failed to update push token`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/profile', async (req, res) => {
	let {driver_id} = req.token;

	try {
		let driver = await Model.Driver.findByPk(driver_id);
		if(driver) {
			return res.json(driver);
		} else {
			return responseUtil.param_error(res, `Driver #${driver_id} is not found`);
		}
	} catch (err) {
		log.error(`Failed to get profile for driver #${driver_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/call', async (req, res) => {
	let { driver_id } = req.token;

	let callsForDriver = await Model.Call.findAll({
		where: {
			driver_ids: { $like: `%#${driver_id}#%`}
		}
	});

	if(callsForDriver.length > 0) {
		let randomIndex = _.random(callsForDriver.length - 1);
		return res.json(callsForDriver[randomIndex]);
	}
	else {
		// Default call
		let defaultCall = await Model.Call.findOne({where: { is_default: true }});
		if(defaultCall) {
			return res.json(defaultCall);
		} else {
			return responseUtil.server_error(res);
		}
	}
});

router.post('/record', upload.any(), async (req, res) => {
	const { driver_id } = req.token;
	
	let audio_file = _.find(req.files, { fieldname: 'audio_file' });
	let video_file = _.find(req.files, { fieldname: 'video_file' });

	if(audio_file == null || video_file == null) {
		return res.status(400).json({msg: "Invalid parameter: Record file is not found"});
	}

	try {
		await tripService.record(driver_id, audio_file, video_file);
		return res.json({success: true});
	} catch (err) {
		log.error(`Failed to record`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/trip', upload.any(), async (req, res) => {
	const { driver_id } = req.token;
	
	let audio_file = _.find(req.files, { fieldname: 'audio_file' });
	let video_file = _.find(req.files, { fieldname: 'video_file' });

	let {
		departure, src_longitude, src_latitude,
		destination, dst_latitude, dst_longitude
	} = req.body;

	if(departure == null) {
		return responseUtil.param_error(res, `"departure" field is not set`);
	} else if(src_latitude == null) {
		return responseUtil.param_error(res, `"src_latitude" field is not set`);
	} else if(src_longitude == null) {
		return responseUtil.param_error(res, `"src_longitude" field is not set`);
	}

	if(destination == null) {
		return responseUtil.param_error(res, `"destinaion" field is not set`);	
	} else if(dst_latitude == null) {
		return responseUtil.param_error(res, `"dst_latitude" field is not set`);
	} else if(dst_longitude == null) {
		return responseUtil.param_error(res, `"dst_longitude" field is not set`);
	}
	
	if(audio_file == null || video_file == null) {
		return res.status(400).json({msg: "Invalid parameter: Record file is not found"});
	}

	try {
		let trip = await Model.Trip.create({
			driver_id,	
			departure, src_longitude, src_latitude,
			destination, dst_latitude, dst_longitude
		});

		if(!trip) {
			return responseUtil.server_error(res);
		}

		await Model.Driver.update({
			status: 'waiting_approval'
		}, { 
			where: { driver_id },
			individualHooks: true
		});
		
		await tripService.record(driver_id, audio_file, video_file, trip.trip_id);
	
		return res.json(trip);
	} catch (err) {
		log.error(`Failed to start trip`, err);
		return responseUtil.server_error(res);
	}
});

router.get('/trip', async(req, res) => {
	const {driver_id} = req.token;

	try {
		let trip = await Model.Trip.findOne({
			where: {
				driver_id
			},
			order: [ [ 'requested_at', 'DESC' ]],
		});
	
		if(trip && !trip.finished) {
			return res.json(trip);
		} else {
			return res.json({});
		}
	} catch (err) {
		log.error(`Failed to get trip`, err);
		return responseUtil.server_error(res);
	}
	
});

router.post('/trip/:trip_id/finish', async(req, res) => {
	const { driver_id } = req.token;
	const { trip_id } = req.params;
	let { arrived } = req.body;

	try {
		let trip = await Model.Trip.findByPk(trip_id);

		if(!trip) {
			return responseUtil.param_error(res, `Trip #${trip_id} is not found`);
		}

		await trip.update({
			finished: true,
			finished_at: new Date(),
			arrived
		});

		await Model.Driver.update({
			status: 'normal'
		}, { 
			where: { driver_id },
			individualHooks: true
		});

		return res.json(trip);
	} catch (err) {
		log.error(`Failed to finish trip #${trip_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.put('/location', async(req, res) => {
	let {driver_id} = req.token;

	let {latitude, longitude} = req.body;

	if(latitude == null || longitude == null) {
		return responseUtil.param_error(res, `"latitude" and "longitude" params are required`);
	}

	try {
		let location = await Model.Location.findByPk(driver_id);
		if(location) {
			await location.update({
				latitude, longitude
			});
		} else {
			location = await Model.Location.create({
				driver_id,
				latitude, longitude
			});
		}
		return res.json(location);
	} catch (err) {
		log.error(`Failed to update driver location, driver: #${driver_id}`, err);
		return responseUtil.server_error(res);
	}
});

router.post('/sos', async(req, res) => {
	let {driver_id} = req.token;

	emergencyService.sos(driver_id);

	return res.json({success: true});
});

router.post('/active', async(req, res) => {
	let {driver_id} = req.token;

	emergencyService.active(driver_id);

	return res.json({success: true});
});

router.post('/ping', async(req, res) => {
	let {driver_id} = req.token;

	emergencyService.ping(driver_id);

	let duration = tripService.getCallDuration();
	return res.json({success: true, duration});
});

router.get('/orders/pending', async (req, res) => {
	let { driver_id } = req.token;

	try {
		let order = await Model.Order.findOne({
			where: {
				driver_id,
				status: 'pending'
			}
		});

		if(order) {
			return res.json(order);
		} else {
			return res.json({
				msg: 'No order exists'
			});
		}
	} catch (err) {
		log.error(`Failed to get pending order`, err);
		return responseUtil.server_error(res);
	}
});


router.post('/orders/:order_id/accept', async (req, res) => {
	let { driver_id } = req.token;
	let { order_id } = req.params;
	let { reject_reason } = req.body;

	try {
		let order = await Model.Order.findByPk(order_id);

		if(!order) {
			return responseUtil.param_error(res, `Order #${order_id} is not found`);
		}

		if(order.driver_id !== driver_id) {
			return responseUtil.param_error(res, `Order #${order_id} doesn't belong to you`);
		}

		await order.update({
			status: 'accepted',
			reject_reason,
			expired_at: new Date()
		});

		return res.json(order);
	} catch (err) {
		log.error(`Failed to accept order`, err);
		return responseUtil.server_error(res);
	}

});


router.post('/orders/:order_id/reject', async (req, res) => {
	let { driver_id } = req.token;
	let { order_id } = req.params;
	let { reject_reason } = req.body;

	try {
		let order = await Model.Order.findByPk(order_id);

		if(!order) {
			return responseUtil.param_error(res, `Order #${order_id} is not found`);
		}

		if(order.driver_id !== driver_id) {
			return responseUtil.param_error(res, `Order #${order_id} doesn't belong to you`);
		}

		await order.update({
			status: 'rejected',
			reject_reason,
			expired_at: new Date()
		});

		return res.json(order);
	} catch (err) {
		log.error(`Failed to reject order`, err);
		return responseUtil.server_error(res);
	}

});

module.exports = router;