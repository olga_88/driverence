const path = require('path');
global.appRoot = path.resolve(__dirname);

switch(process.env.NODE_ENV) {
  case 'development': {
    console.info(`
===============================================================
=====================Development Environment===================
===============================================================
`);
    require('dotenv').config({path: '.env.development'});
    break;
  }
  case 'production': {
    console.info(`
===============================================================
=====================Production Environment====================
===============================================================
`);
    require('dotenv').config({path: '.env'});
    break;
  }
  default: {
    console.error(`Unknown environment: ${process.env.NODE_ENV}`);
    process.exit(1);
  }
}

const _ = require('lodash');
const debug = require('debug');
let tags = [process.env.DEBUG];

let log_levels = _.dropWhile(['debug', 'info', 'warn', 'error'], level => level != (process.env.LOG_LEVEL || 'debug'));
tags.push(log_levels.map(l => `*:${l}`));
debug.enable(tags.join(','));

require('./server');
  
  