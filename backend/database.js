const Sequelize = require("sequelize");const Op = Sequelize.Op;

const operatorsAliases = {
  $eq: Op.eq,
  $ne: Op.ne,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col,
  $like: Op.like,
  $regexp: Op.regexp,
  $in: Op.in
};


let {DB_NAME, DB_USER, DB_PASS, DB_HOST, DB_DIAL} = process.env;
let database = new Sequelize(
  DB_NAME, DB_USER, DB_PASS, {
    host: DB_HOST,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
    define: {
      underscored: true
    },
    logging: false,
    operatorsAliases
  }
);

module.exports = database;