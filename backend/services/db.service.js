//@ts-check
const _ = require('lodash');
const log = require('./logger.service')('DBService');
const database = require('../database');
const { Subject } = require('rxjs');
const Models = require('../models');

const dbService = () => {
  const _tripSubject = new Subject();
  const _messageSubject = new Subject();
  const _driverSubject = new Subject();
  const _locationSubject = new Subject();
  const _recordSubject = new Subject();

  const subscriptions = {
    trip: _tripSubject.asObservable(),
    message: _messageSubject.asObservable(),
    driver: _driverSubject.asObservable(),
    location: _locationSubject.asObservable(),
    record: _recordSubject.asObservable(),
  };

  const initAssociations = () => {
    Models.Trip.belongsTo(Models.Driver, {foreignKey: 'driver_id'});
    Models.Driver.hasMany(Models.Trip, {foreignKey: 'driver_id', sourceKey: 'driver_id'});

    Models.Record.belongsTo(Models.Driver, {foreignKey: 'driver_id'});
    Models.Driver.hasMany(Models.Record, {foreignKey: 'driver_id', sourceKey: 'driver_id'});
  }

  const initSubscriptions = () => {
    Models.Driver.addHook('afterSave', (driver) => {
      _driverSubject.next(_.omit(driver.toJSON(), ['password']));
    });

    Models.Location.addHook('afterSave', (location) => {
      _locationSubject.next(location.toJSON());
    });

    Models.Message.addHook('afterSave', (message) => {
      _messageSubject.next(message.toJSON());
    });

    Models.Trip.addHook('afterSave', (trip) => {
      _tripSubject.next(trip.toJSON());
    });

    Models.Record.addHook('afterSave', (record) => {
      _recordSubject.next(record.toJSON());
    });
  }

  const start = async () => {
    try {
      await database.authenticate();
      initAssociations();
      initSubscriptions();
      await database.sync();
      log.info('Connection to the database has been established successfully');
    } catch (err) {
      log.error(`Unable to connect to database, ${err}`);
			return process.exit(1);
    }
  };

  return {
    start,
    subscriptions
  };
};

module.exports = dbService();
