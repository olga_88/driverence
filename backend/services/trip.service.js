const log = require('./logger.service')('TripService');
const fcmService = require('./fcm.service');
const Model = require('../models');
const path = require('path');
const analyzeService = require('./analyze.service');
const moment = require('moment');

const tripService = () => {
    const minDuration = moment.duration(20, 'm').asMilliseconds();
    let duration = minDuration;

    let record = async (driver_id, audio_file, video_file, trip_id) => {
        let audio_file_path = `${audio_file.destination}/${audio_file.filename}`;
		let video_file_path = `${video_file.destination}/${video_file.filename}`;

        let driver = await Model.Driver.findByPk(driver_id);
        if(!driver) {
            return;    
        }

        let record = await Model.Record.create({
            driver_id,
            audio_file_path,
            video_file_path
        });
	
        let audio_full_path =  path.join(global.appRoot, audio_file.path);
		// start analyzing
		analyzeService.analyze(audio_full_path).then(({report, error}) => {
            if(error == null) {
                let passed = report.tags.length === 0;
                let result = passed ? 'OK' : report.tags.join(', ');

                Model.Record.update({
                    analysis_result: result,
                    analysis_report: report
                }, { 
                    where: { 
                        record_id: record.record_id, 
                        driver_id 
                    },
                    individualHooks: true
                });

                if(!passed) {
                    log.db({
                        type: 'alert',
                        category: 'risk',
                        publisher_name: "Analyzing Service",
                        content: `${driver.full_name}(@${driver.user_name}) is suspicious. ${result}`
                    });
                } else if(trip_id) {
                    // Auto approve the trip
                    approve(trip_id, 0);
                }
            } else {
                let result = error;
                Model.Record.update({
                    analysis_result: result,
                    analysis_report: report
                }, { 
                    where: { 
                        record_id: record.record_id, 
                        driver_id 
                    },
                    individualHooks: true
                });
            }
		}).catch(err => {
			log.error(`Failed to analyze record, ${audio_full_path}`, err);
		})

        return record;
    };

	let approve = async (trip_id, reviewer_id) => {
        let trip = await Model.Trip.findByPk(trip_id);
        if(!trip) {
            throw new Error(`Trip #${trip_id} not found`);
        }

        let driver_id = trip.driver_id;
        let driver = await Model.Driver.findByPk(driver_id);
        if(!driver) {
            throw new Error(`Driver #${driver_id} not found`);
        }

        await Model.Trip.update({
            approved: true,
            reviewed_at: new Date(),
            reviewer_id
        }, {
            where: { trip_id },
            individualHooks: true
        });
        
        await Model.Driver.update({ 
            status: 'on_trip' 
        }, {
            where: { driver_id },
            individualHooks: true
        });

        log.db({
            type: 'log',
            publisher_name: "Admin Service",
            content: `Driving request #${trip_id} from ${driver.full_name}(@${driver.user_name}) has been approved.`,
            category: 'approved',
            meta_data: {
                trip_id,
                reviewer_id,
                destination: trip.destination
            }
        });
        
		fcmService.sendDataMessage(driver.push_token, {
            cmd: "approval",
			approved: "true",
			destination: trip.destination
		});
    };
    
    let reject = async (trip_id, reject_reason, reviewer_id) => {
        let trip = await Model.Trip.findByPk(trip_id);
        if(!trip) {
            throw new Error(`Trip #${trip_id} not found`);
        }

        let driver_id = trip.driver_id;
        let driver = await Model.Driver.findByPk(driver_id);
        if(!driver) {
            throw new Error(`Driver #${driver_id} not found`);
        }

        await Model.Trip.update({
            approved: false,
            reviewed_at: new Date(),
            reviewer_id: reviewer_id,
            reject_reason,

            finished_at: new Date(),
            finished: true,
            arrived: false
        }, {
            where: { trip_id },
            individualHooks: true
        });
        
        await Model.Driver.update({ 
            status: 'normal' 
        }, {
            where: { driver_id },
            individualHooks: true
        });

        log.db({
            type: 'log',
            publisher_name: "Admin Service",
            content: `Driving request #${trip_id} from ${driver.full_name}(@${driver.user_name}) has been rejected.`,
            category: 'rejected',
            meta_data: {
                trip_id,
                reviewer_id,
                destination: trip.destination
            }
        });
	
		fcmService.sendDataMessage(driver.push_token, {
            cmd: "approval",
			approved: "false",
			reject_reason,
			destination: trip.destination
		});
    }

    let setCallDuration = (value) => {
        duration = Math.max(minDuration, value);
        log.debug(`Update call duration: ${duration}`);
        return duration;
    }

    let getCallDuration = () => {
        return duration;
    }


    let sendOrder = async (order) => {
        let { driver_id, destination } = order;

        try {
            let driver = await Model.Driver.findByPk(driver_id);

            if(!driver) {
                throw new Error(`Driver #${driver_id} not found`);
            }

            let push_token = driver.push_token;

            if(!push_token) {
                throw new Error(`Push token not found`);
            }

            fcmService.sendDataMessage(push_token, {
                cmd: "trip_order",
                destination: destination
            });
        } catch (err) {
            log.error(err);
        }
        
    }
	
	return {
        setCallDuration,
        getCallDuration,
        record,
        approve,
        reject,
        sendOrder
	}
};

module.exports = tripService();