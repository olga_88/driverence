const crypto = require('crypto');

const hashService = () => {
  const password = (pw) => {
    return crypto.createHash('md5').update(pw).digest('hex');
  };

  const comparePassword = (pw, hash) => {
    return password(pw) === hash;
  };

  return {
    password,
    comparePassword
  };
};

module.exports = hashService();
