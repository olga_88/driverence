const nodemailer = require('nodemailer');
const log = require('./logger.service')('mail-service');

const mailService = () => {
  let transporter = nodemailer.createTransport({
    sendmail: true,
    newline: 'unix',
    path: '/usr/sbin/sendmail'
  });

	let sendMail = (from, to, subject, text) => {
    try {
      log.debug(`Sending mail to ${to}...`);

      if(process.env.NODE_ENV === 'production') {
        transporter.sendMail({
          from,
          to,
          subject,
          text
        }, (err, info) => {
          if(err) {
            log.error(err);
          } else {
            log.info(info.envelope);
            log.info(info.messageId);
          }
        });
      } else {
        log.info(`to ${to}`, subject, text);
      }
    } catch (err) {
      log.error(err);
    }
		
	};
	
	return {
		sendMail,
	}
};

module.exports = mailService();