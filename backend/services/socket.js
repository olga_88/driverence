const authService = require('./auth.service');
const log = require('./logger.service')('SocketService');

const ADMIN_CHANNEL = 'admin';

const SocketService = (server) => {
    const start = async () => {
		  let io = require('socket.io')(server);
          io.use(function(socket, next) {
            let query = socket.handshake.query;
            if(query && query.token) {
              	authService.verify(query.token, (err, token) => {
					if(err) {
						return next(new Error(`Failed to access websocket, error: ${err}`));
					} else if(token.role !== 'admin') {
						return next(new Error(`Authorization failed. Admin role is required`));
					} else {
						next();
					}
              })
            } else {
              	return next(new Error(`Authorization is not provided`));
            }
          })
        .on('connection', async socket => {
            log.info(`new connection has been established`);
        
        	socket.join(ADMIN_CHANNEL);
        });
	 
		
		log.info('Socket service has been started');
		global.socketio = io;
    }

    return {
        start
    }
};

module.exports = SocketService;