const FCM = require('fcm-node');

const fcm = new FCM(process.env.FCM_SERVER_KEY);
const log = require('./logger.service')('fcm');

const fcmService = () => {
	let sendDataMessage = (device_token, data, expiration = 60) => {
		log.debug(`Sending message to ${device_token}...`);

		let message = {
			to: device_token,
			data,
			time_to_live: expiration
		}

		fcm.send(message, (err, response) => {
			if(err) log.error(`Failed to send message to ${device_token}`, err);
		});
	};
	
	return {
		sendDataMessage,
	}
};

module.exports = fcmService();