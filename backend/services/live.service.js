//@ts-check
const log = require('./logger.service')('LiveService');
const fcmService = require('./fcm.service');
const Model = require('../models');
const crypto = require('crypto');

const liveService = () => {
    let getRandomHash = (seed) => {
        let getRndInteger = (min, max) => {
            return Math.floor(Math.random() * (max - min) ) + min;
        }

        return crypto.createHash('sha1').update(`#${seed}#` + getRndInteger(0, 10000)).digest('hex');
    }

    /** @param {number}  driver_id */
    let getPushToken = async (driver_id) => {
        let driver = await Model.Driver.findByPk(driver_id);
        if(!driver) {
            log.warn(`Driver #${driver_id} not found`);
            return null;
        }

        return driver['push_token'];
    }
    
    /**
     * @param {number} driver_id 
     * @param {string} stream_type cam1|cam2|screen
     */
    let startLiveStream = async (driver_id, stream_type) => {
        if(driver_id == null) {
            log.warn(`Driver id is not defined`);
            return;
        }

        if(stream_type == null) {
            log.warn(`Stream type is not defined`);
            return;
        }

        let token = await getPushToken(driver_id);
        if(!token) {
            log.warn(`Token is empty`);
            return;
        }

        log.debug(`token: ${token}`);

        let room_id = getRandomHash(driver_id);

        fcmService.sendDataMessage(token, {
            cmd: "start_live",
            room_id,
            stream_type
        });

        return room_id;
    }

    /**
     * @param {number} driver_id 
     */
    let stopLiveStream = async (driver_id) => {
        if(driver_id == null) {
            log.warn(`Driver id is not defined`);
            return;
        }

        let token = getPushToken(driver_id);
        if(!token) {
            log.warn(`Token is empty`);
            return;
        }

        fcmService.sendDataMessage(token, {
            cmd: "stop_live",
        });
    }
	
	return {
        getRandomHash,
        startLiveStream
	}
};

module.exports = liveService();