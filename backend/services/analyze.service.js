//@ts-check
const axios = require('axios');
const fs = require('fs');
const FormData = require('form-data');
const _ = require('lodash');
const path = require('path');
const { fileURLToPath } = require('url');
const log = require('./logger.service')('AnalyzeService')

const analyzeService = () => {
    const upload2fileio = async (file_path) => {
        let formData = new FormData();
        formData.append("file", fs.createReadStream(file_path));

        const res = await axios.default.post("https://file.io", formData, {
            headers: formData.getHeaders()
        });

        const {success, link} = res.data;

        return {
            success, link
        };
    }

    const upload2lva7 = (file_path) => {
        let filename = path.basename(file_path);
        let destination  = path.join(process.env.LVA7_PATH, 'records', filename);
        log.debug(`Copying record file to lva7 folder, destination: ${destination}`);

        fs.copyFileSync(file_path, destination);
        return `/records/${filename}`;
    }

    const removeLva7File = (file_path) => {
        try {
            let destination = path.join(process.env.LVA7_PATH, file_path);
            log.debug(`Deleting record file in lva7 folder, file: ${destination}`);
            fs.unlinkSync(destination);
        } catch (e) {
            log.error(`Failed to remove lva7 file ${file_path}`);
        }
    }

    const get_lva7 = async (file_url) => {
        let query = `?action=ANALYZE&url=${encodeURIComponent(file_url)}&filename=sample`;
        let url = "https://panel.whencyber.com/LVA7/_api.php" + query;
        let api_key = process.env.LVA7_API_KEY;

        log.debug(`Querying lva7 report, url: ${url}`);

        const res = await axios.default.post(url, null, {
            headers: {
                api_key
            }
        });
        return res.data;
    }

    const analyze_lva7 = (json) => {
        log.debug(`LVA7 report analyzing started...`);
        let segments = _.filter(json['Segments'], { Channel: "0" });

        /** 
         * @param {Object[]} array 
         * @param {string} field
         * @returns {number} average
         **/
        const getAverage = (array, field) => {
            if(array.length === 0) return 0;
            let sum = _.sumBy(array, obj => Number(obj[field]));
            return sum / array.length;
        };

        let SPT = getAverage(segments, 'SPT');
        let SPJ = getAverage(segments, 'SPJ');
        let OCA = getAverage(segments, 'O.C.A');
        let Energy = getAverage(segments, 'Energy');
        let JQ = getAverage(segments, 'JQ');
        let LJ = getAverage(segments, 'LJ');
        let hJQ = getAverage(segments, 'hJQ');
        let AVJ = getAverage(segments, 'AVJ');
        let Stress = getAverage(segments, 'Stress');

        const NMS_SD_NOSTRESS=1;
        const NMS_SD_LOW=2;
        const NMS_SD_MID_TEMP_OK=3;
        const NMS_SD_HIGH_TEMP_OK=4;
        const NMS_SD_MID_WARN=5;
        const NMS_SD_HIGH_WARN=6;
        const NMS_SD_DANGERLEVEL=7;

        let CLStress = NMS_SD_NOSTRESS;
        if(JQ > 49) CLStress = NMS_SD_DANGERLEVEL;
        else if(JQ > 47) CLStress = NMS_SD_HIGH_WARN;
        else if(JQ > 45) CLStress = NMS_SD_MID_WARN;
        else if(JQ > 35) CLStress = NMS_SD_LOW;

        let report = {SPT, SPJ, OCA, Energy, JQ, LJ, hJQ, AVJ, Stress, CLStress};

        let tags = [];
        if(Energy <= 2) tags.push('Fatigue risk is high.');
        if(CLStress >= NMS_SD_MID_WARN) tags.push('Extreme stress.');
        if(Energy > 15) tags.push('Extreme energy - suspected speeders.');
        if(AVJ > 1400 || LJ > 24) tags.push('Extreme COG state - Suspect drug use.');
        if(AVJ < 220) tags.push('Suspected alchol use.');

        report.tags = tags;

        log.debug('lva7 analysis report:', report);

        return report;
    }

    /**
     * @param {string} file_path 
     */
    const analyze = async (/** @type {string}} **/file_path) => {
        if(process.env.NODE_ENV === 'production') {
            let file_url = upload2lva7(file_path);
            let json = await get_lva7(file_url);
            removeLva7File(file_url);

            if(json['Status'] !== 'OK') {
                let error = 'Failed to analyze file. ' + json['Error Message'];
                return { error };
            }

            let report = analyze_lva7(json);
            return { report };
        } else {
            // Method 1. use real analysis result
            // let { link: file_url }  = await upload2fileio(file_path);
            // if(file_url) {
            //     return _analyze(file_url);
            // }

            // Method 2. use mocked analysis result

            // Randomly analyze failed error
            if(_.random(0, 10) === 4) return {
                error: `Failed to analyze record`
            };

            let tags = [ 
                'Fatigue risk is high',
                'Extreme stress',
                'Extreme energy - suspected speeders use' ,
                'Extreme COG state - Suspect drug use',
                'Suspected alchol use',
            ]
            
            let report = {
                SPT: _.random(10), 
                SPJ: _.random(10), 
                OCA: _.random(10), 
                Energy: _.random(10), 
                JQ: _.random(10), 
                LJ: _.random(10), 
                hJQ: _.random(10), 
                AVJ: _.random(10), 
                Stress: _.random(10), 
                CLStress: _.random(10), 
            };

            report.tags = _.random(1) === 0 ? [tags[_.random(4)]] : [];

            return { report };
        }
    }

    return {
        analyze
    }
};

module.exports = analyzeService();