
const axios = require('axios');
const log = require('./logger.service')('telegram');

const telegramService = () => {
    let CHANNEL_ID = process.env.TELEGRAM_CHANNEL_ID;
    let BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN;

    let sendChannelMessage = async (channel_id, text) => {
        let url = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage`;
        let data = {
            chat_id: channel_id,
            text
        };

        let res = await axios.default.post(url, data);
    }

	let sendMessaage = async (text) => {
        if(CHANNEL_ID == null || BOT_TOKEN == null) {
            log.warn(`Telegram token is not specified`);
            return;
        }

        log.debug(`Sending telegram message...`);
        sendChannelMessage(CHANNEL_ID, text);
	};
	
	return {
		sendMessaage,
	}
};

module.exports = telegramService();