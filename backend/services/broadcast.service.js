//@ts-check
const log = require('./logger.service')('BroadcastService');

const io = require('socket.io')

const dbService = require('./db.service');
const emergencyService = require('./emergency.service');

const ADMIN_CHANNEL = "admin";

const ADMIN_EVENT_DRIVER = "driver"
const ADMIN_EVENT_LOCATION = "location"
const ADMIN_EVENT_MESSAGE = "message";
const ADMIN_EVENT_TRIP = "trip";
const ADMIN_EVENT_STATUS = "status";
const ADMIN_EVENT_RECORD = "record";

class BroadcastService {
    /**
     * @returns {io.Server}
     */
    get socket() {
        //@ts-ignore
        return global.socketio;
    }

    get adminChannel() {
        return this.socket.to(ADMIN_CHANNEL);
    }

    publishDriver(driver) {
        if(this.adminChannel)
            this.adminChannel.emit(ADMIN_EVENT_DRIVER, driver);
    }

    publishLocation(location) {
        if(this.adminChannel)
            this.adminChannel.emit(ADMIN_EVENT_LOCATION, location);
    }

    publishMessage(message) {
        if(this.adminChannel)
            this.adminChannel.emit(ADMIN_EVENT_MESSAGE, message);
    }

    publishTrip(trip) {
        if(this.adminChannel)
            this.adminChannel.emit(ADMIN_EVENT_TRIP, trip);
    }

    publishStatus(status) {
        if(this.adminChannel)
            this.adminChannel.emit(ADMIN_EVENT_STATUS, status);
    }

    publishRecord(record) {
        if(this.adminChannel)
            this.adminChannel.emit(ADMIN_EVENT_RECORD, record);
    }

    start() {
        dbService.subscriptions.driver.subscribe(driver => this.publishDriver(driver));
        dbService.subscriptions.location.subscribe(location => this.publishLocation(location));
        dbService.subscriptions.message.subscribe(message => this.publishMessage(message));
        dbService.subscriptions.trip.subscribe(trip => this.publishTrip(trip));
        dbService.subscriptions.record.subscribe(record => this.publishRecord(record));

        emergencyService.statusObservable.subscribe(status => this.publishStatus(status));

        log.info(`Service started`);
    }
}
module.exports = new BroadcastService();