//@ts-check
const WebSocket = require('ws');
const _ = require('lodash');
const url = require('url');

const log = require('../services/logger.service')('SignalServer');
const TYPE_CALL = "call";
const TYPE_ANSWER = "answer";
const TYPE_LOCAL_CANDIDATE = "localIceCandidate";
const TYPE_REMOTE_CANDIDATE = "remoteIceCandidate";
const TYPE_ERROR = "error";
const TYPE_REMOTE_SDP = "sdp";
const TYPE_LISTENER_READY = "listenerReady";
const TYPE_LISTENER_LEAVE = "listenerLeave";

const SDP_TYPE_OFFER = 'offer';
const SDP_TYPE_ANSWER = 'answer';

/** 
 * @typedef {Object} ClientObject 
 * @property {any[]} [candidateQueue]
 * @property {WebSocket} [ws]
 */

class Room {
  /**
   * 
   * @param {string} roomId 
   */
  constructor(roomId) {
    /** @type {string} */
    this.roomId = roomId;

    /** @type {WebSocket.Server} */
    this.wsServer = new WebSocket.Server({noServer: true});

    /** @type {ClientObject} */
    this.presenter = { candidateQueue: [] };

    /** @type {ClientObject} */
    this.viewer = { candidateQueue: []};
  }

  /**
   * Get room if exist, otherwise return new room
   * @param {string} roomId 
   */
  static getRoom(roomId) {
    var room = _.find(Room.activated, {roomId: roomId});
    if(!room) {
      room = new Room(roomId);
      Room.activated[roomId] = room;
    }

    return room;
  }

  /**
   * @returns {WebSocket.Server}
   */
  get ws() {
    return this.wsServer;
  }

  /**
   * @returns {string} 
   */
  get roomName() {
    return `${this.roomId.toUpperCase()}`;
  }

  /**
   * presenter websocket
   * @param {WebSocket} ws 
   */
  create(ws) {
    log.info(this.roomName, `Presenter created this room`);

    this.presenter.ws = ws;

    if(this.viewer.ws) {
      this.presenter.ws.send(JSON.stringify({
        type: TYPE_LISTENER_READY
      }));
    }

    ws.on('message', async (message) => {
      if(typeof message === 'string') {
        var json;
        try {
          json = JSON.parse(message);
        } catch (err) {
          log.warn(`Error while parsing message from presenter. Ignored`);
          return;
        }

        // log.debug(`Message from presenter, ${message}`);

        switch (json.type) {
          case TYPE_CALL: {
            if(!this.viewer.ws) {
              ws.send(JSON.stringify({
                type: TYPE_ERROR,
                message: 'Viewer is absent'
              }));
              return;
            }

            /** @type {{sdpOffer: string}} */
            var call = json;
            this.viewer.ws.send(JSON.stringify({
              type: TYPE_REMOTE_SDP,
              sdpType: SDP_TYPE_OFFER,
              sdp: call.sdpOffer
            }));
            break;
          }
          case TYPE_ANSWER: {
            if(!this.viewer.ws) {
              ws.send(JSON.stringify({
                type: TYPE_ERROR,
                message: 'Viewer is absent'
              }));
              return;
            }

            /** @type {{sdpAnswer: string}} */
            var answer = json;
            this.viewer.ws.send(JSON.stringify({
              type: TYPE_REMOTE_SDP,
              sdpType: SDP_TYPE_ANSWER,
              sdp: answer.sdpAnswer
            }));
            break;
          }
          case TYPE_LOCAL_CANDIDATE: {
            log.info(this.roomName, `Received Ice-Candidate from presenter`);

            /** @type {{candidate: any}} */
            var localCandidate = json;
            if(this.viewer.ws) {
              log.debug(this.roomName, `Sending presenter candidate to viewer`);
              this.viewer.ws.send(JSON.stringify({
                type: TYPE_REMOTE_CANDIDATE,
                candidate: localCandidate.candidate
              }));
            } else {
              log.debug(this.roomName, `Queueing presenter candidate`);
              this.presenter.candidateQueue.push(localCandidate.candidate);
            }
            break;
          }
        }
      }
    });

    ws.on('error', (err) => {
      log.error(this.roomName, `WS error for presenter, ${err.message}`);
      this.closePresenter();
    });
    
    ws.on('close', (code, reason) => {
      log.debug(this.roomName, `Presenter is leaving room, code = ${code}`);
      this.closePresenter();
    });
  }

  closePresenter() {
    this.presenter.ws.close();
    this.presenter = { candidateQueue: []};

    if(!this.viewer.ws) {
      this.close();
    }
  }

  /**
   * viewer websocket
   * @param {WebSocket} ws 
   */
  join(ws) {
    if(this.viewer.ws != null) {
      ws.close(1007, 'Tray after few seconds.');
      return this.closeViewer();
    }

    // block viewer to join room until presenter is present
    this.viewer.ws = ws;

    log.info(this.roomName, `Viewer joined room`);

    if(this.presenter.ws) {
      this.presenter.ws.send(JSON.stringify({
        type: TYPE_LISTENER_READY
      }));
    }

    ws.on('message', async (message) => {
      if(typeof message === 'string') {
        var json;
        try {
          json = JSON.parse(message);
        } catch (err) {
          log.warn(`Error while parsing message from presenter. Ignored`);
          return;
        }
        // log.debug(`Message from viewer, ${message}`);

        switch (json.type) {
          case TYPE_CALL: {
            if(!this.presenter.ws) {
              ws.send(JSON.stringify({
                type: TYPE_ERROR,
                message: 'Presenter is absent'
              }));
              return;
            }

            /** @type {{sdpOffer: string}} */
            var call = json;
            this.presenter.ws.send(JSON.stringify({
              type: TYPE_REMOTE_SDP,
              sdpType: SDP_TYPE_OFFER,
              sdp: call.sdpOffer
            }));
            break;
          }
          case TYPE_ANSWER: {
            if(!this.presenter.ws) {
              ws.send(JSON.stringify({
                type: TYPE_ERROR,
                message: 'Presenter is absent'
              }));
              return;
            }

            /** @type {{sdpAnswer: string}} */
            var answer = json;
            this.presenter.ws.send(JSON.stringify({
              type: TYPE_REMOTE_SDP,
              sdpType: SDP_TYPE_ANSWER,
              sdp: answer.sdpAnswer
            }));
            break;
          }
          case TYPE_LOCAL_CANDIDATE: {
            log.info(this.roomName, `Received Ice-Candidate from viewer`);

            /** @type {{candidate: any}} */
            var localCandidate = json;
            if(this.presenter.ws) {
              log.debug(this.roomName, `Sending viewer candidate to presenter`);
              this.presenter.ws.send(JSON.stringify({
                type: TYPE_REMOTE_CANDIDATE,
                candidate: localCandidate.candidate
              }));
            } else {
              log.debug(this.roomName, `Queueing viewer candidate`);
              this.viewer.candidateQueue.push(localCandidate.candidate);
            }
            break;
          }
        }
      }
    });

    ws.on('error', (err) => {
      log.error(this.roomName, `WS error for viewer, ${err.message}`);
      this.closeViewer();
    });

    ws.on('close', (code, reason) => {
      log.debug(this.roomName, `Viewer is leaving room, code = ${code}`);
      this.closeViewer();
    });
  }

  /**
   * 
   * @param {number} [code]
   * @param {string} [reason]
   */
  closeViewer(code, reason) {
    this.viewer.ws.close(code, reason);
    this.viewer = { candidateQueue: []};
    if(!this.presenter.ws) {
      this.close();
    } else {
      this.presenter.ws.send(JSON.stringify({
        type: TYPE_LISTENER_LEAVE
      }));
    }
  }

  /**
   * Close Room
   */
  close() {
    // close ws server
    this.ws.close();

    // remove from activated room
    delete Room.activated[this.roomId];
  }
}

/** @type {{[roomId: string] : Room}} */
Room.activated = {};

const webRtcService = (server) => {
  let start = () => {
    log.info(`Service started`);

    server.on('upgrade', (req, socket, head) => {
      try {
        const urlData = url.parse(req.url, true);
        const query = urlData.query;
        const paths = urlData.pathname.split('/').filter(x => x);
        if(paths.length === 3 && paths[0] === 'broadcast') {

          /** @type {string} */
          //@ts-ignore
          const sessionId = query['sessionId'];

          if(!sessionId) {
            throw new Error('no session id in query param');
          }

          const action = paths[1];
          const roomId = paths[2];
          switch (action.toLocaleLowerCase()) {
            case 'create': {
              const room = Room.getRoom(roomId);
              room.ws.handleUpgrade(req, socket, head, (ws) => {
                room.create(ws);
              });
              break;
            }
            case 'join': {
              const room = Room.getRoom(roomId);
              room.ws.handleUpgrade(req, socket, head, (ws) => {
                room.join(ws);
              });
              break;
            }
            default: {
              throw new Error(`Unknown action ${action}`);
            }
          }
        } else {
          // throw new Error(`unexpected ws endpoint: ${urlData.pathname}`);
        }
      } catch (e) {
        log.error(e.message);
      }
    });
  }

  return {
    start
  }
}

module.exports = webRtcService;
