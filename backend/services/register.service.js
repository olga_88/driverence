//@ts-check
const log = require('./logger.service')('register-service');
const crypto = require('crypto');
const validator = require('validator').default;
const Model = require('../models');
const mailService = require('./mail.service');

const registerService = () => {
  let randomPassword = () => {
    return crypto.randomBytes(20).toString('hex');
  }

  function randomToken(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  let sendResetCode = async (email) => {
    let token = randomToken(6);
    await Model.ResetPwdToken.create({
      email,
      token
    });

    let from = process.env.SUPPORT_MAIL;
    let content = 
` 
Your reset password code is ${token}.
`;
    mailService.sendMail(from, email, 'Reset password', content);
    return;
  }

  let sendInvitation = async (email) => {
    let token = randomToken(6);
    await Model.ResetPwdToken.create({
      email,
      token
    });

    let from = process.env.SUPPORT_MAIL;
    let downloadLink = new URL(`/api/assets/uploads/apps/latest?ref=${token}`, process.env.APP_HOST).href;
    let content = 
` 
Your registeration code is ${token}. 
You can download Android app in this url ${downloadLink}
`;
    mailService.sendMail(from, email, 'Join us', content);
    return;
  };

  let registerDriver = async ({
    user_name, first_name, last_name, email, phone_number, imei, company_id, password, preregister
  }) => {
    try {
      // validate email
      if(!validator.isEmail(email)) {
        throw new Error(`Invalid email format`);
      }

      if(!validator.isMobilePhone(phone_number)) {
        throw new Error(`Invalid phone number format`);
      }

      if(await Model.User.count({where: { user_name }}) > 0) {
        throw new Error('Username is already taken.');
      }

      if(await Model.User.count({where: { email }}) > 0) {
        throw new Error('Email is already taken.');
      }

      if(await Model.Driver.count({where: { email }}) > 0) {
        throw new Error('Email is already taken.');
      }
  
      //@ts-ignore
      let user_id = await Model.User.lastUserId() + 1;
      let user = await Model.User.create({
        user_id,
        user_name,
        password: randomPassword(),
        first_name,
        last_name,
        email,
        phone_number
      });
  
      if(!user) {
        throw new Error('Failed to create user.');
      }
  
      let driver = await Model.Driver.create({
        user_id,
        user_name,
        first_name,
        last_name,
        email,
        password: password ? password : crypto.randomBytes(20).toString('hex'),
        phone_number,
        company_id,
        imei
      });
  
      if(!driver) {
        throw new Error('Failed to create driver.');
      }

      if(preregister) {
        sendInvitation(email);
      }
  
      return {
        user,
        driver
      };
    } catch (err) {
      return {
        error: err.message
      };
    }
    
  }

  return {
    registerDriver,
    sendInvitation,
    sendResetCode
  }
};

module.exports = registerService();