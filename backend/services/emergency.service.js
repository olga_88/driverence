//@ts-check
const _ = require('lodash');
const moment = require('moment');
const { headingDistanceTo, distanceTo } = require('geolocation-utils');

const { Subject, Observable } = require('rxjs');

const telegramService = require('./telegram.service');
const Model = require('../models');

const log = require('./logger.service')('EmergencyService');
const dbService = require('./db.service');
const Models = require('../models');

const StatusTag = {
    Normal: 'normal',
    WrongWay: 'wrong_way',
    Stopped: 'stopped',
    ConnectionLost: 'connection_lost',
    Emergency: 'emergency'
};

const CHECK_STATUS_INTERVAL = moment.duration(1, 'minute').asMilliseconds();

const KEEP_LOCATIONS_DURATION = moment.duration(5, 'minutes').asMilliseconds();
const CONNECTION_LOST_MAX = moment.duration(1, 'minute').asMilliseconds();


class MonitorService {
    /** 
     * @typedef {Object} DriverStatus
     * @property {number} driver_id Driver's ID
     * @property {string} tag
     * @property {number} updated_at
     */

    /**
     * @typedef {Object} DriverConnection
     * @property {number} driver_id
     * @property {number} [last_ping_at]
     */

    /**
     * @typedef {Object} DriverMovement
     * @property {number} driver_id
     * @property {{latitude: number, longitude: number}} departure
     * @property {{latitude: number, longitude: number}} destination
     * @property {boolean} checkable
     * @property {{latitude: number, longitude: number, timestamp: number}[]} locations 
     */

    constructor() {
        /** @type {number[]} ID array of drivers on trip */
        this.driversOnTrip = [];

        /** @type {DriverStatus[]} */
        this.statuses = [];

        /** @type {DriverMovement[]} */
        this.movements = [];

        /** @type {DriverConnection[]} */
        this.connections = [];

        /** @type {Subject<DriverStatus>} */
        this.statusSubject = new Subject();
    }

    /**
     * @param {number} driver_id
     * @returns {DriverStatus}
     */
    getStatus(driver_id) {
        return _.find(this.statuses, { driver_id });
    }

    /**
     * @param {number} driver_id 
     * @returns {DriverMovement}
     */
    getMovement(driver_id) {
        return _.find(this.movements, { driver_id });
    }

    /**
     * @param {number} driver_id 
     * @returns {DriverConnection}
     */
    getConnection(driver_id) {
        return _.find(this.connections, { driver_id });
    }

    /**
     * 
     * @param {number} driver_id 
     * @param {string} tag 
     */
    async reportStatus(driver_id, tag) {
        let status = this.getStatus(driver_id);

        if(status.tag === tag 
            && tag !== StatusTag.Emergency) { // SOS could be duplicated
            return;
        }

        let now = moment.now();

        status.tag = tag;
        status.updated_at = now;

        let driver = await Models.Driver.findByPk(driver_id);

        let full_name = driver['full_name'];
        let user_name = driver['user_name'];

        if(tag === StatusTag.Emergency) {
            let content = `SOS has been called by ${full_name}(@${user_name})`;
                        
            log.db({
                type: 'alert',
                content,
                category: 'sos',
                publisher_name: "Emergency Service",
                meta_data: {
                    driver_id,
                }
            });
    
            telegramService.sendMessaage(content);
        } else if(tag === StatusTag.ConnectionLost) {
            log.warn(`Connection to ${full_name}(@${driver['user_name']}) has been lost`);
            log.db({
                type: "alert",
                category: "risk",
                publisher_name: "Emergency Service",
                content: `Connection to ${full_name}(@${driver['user_name']}) has been lost`,
                meta_data: {}
            });
        } else if(tag === StatusTag.Stopped) {
            log.warn(`${full_name}(@${driver['user_name']}) has been stopped driving.`);
            log.db({
                type: "alert",
                category: "risk",
                publisher_name: "Emergency Service",
                content: `${full_name}(@${driver['user_name']}) has been stopped driving`,
                meta_data: {}
            });
        } else if(tag === StatusTag.WrongWay) {
            log.warn(`${full_name}(@${driver['user_name']}) is going the wrong way.`);
            log.db({
                type: "alert",
                category: "risk",
                publisher_name: "Emergency Service",
                content: `${full_name}(@${driver['user_name']}) is going the wrong way`,
                meta_data: {}
            });
        } else if(tag === StatusTag.Normal) {
            // 
        }

        this.statusSubject.next(status);
    }

    /**
     * @param {string|number} latitude
     * @param {string|number} longitude
     */
    toLatLon(latitude, longitude) {
        return {lat: Number(latitude), lon: Number(longitude)};
    }

    checkStatus() {
        log.debug(`>>>>>>> Checking driver status.`, '# of drivers on trip:', this.driversOnTrip.length);
        let now = moment.now();
        _.forEach(this.driversOnTrip, driver_id => {
            let status = this.getStatus(driver_id);
            let connection = this.getConnection(driver_id);
            let movement = this.getMovement(driver_id);

            if(!status) return;

            if(status.tag === StatusTag.Emergency) return;

            if(connection) {
                if( !connection.last_ping_at ||  (now - connection.last_ping_at) > CONNECTION_LOST_MAX) {
                    return this.reportStatus(driver_id, StatusTag.ConnectionLost);
                }
            }

            if(movement && movement.checkable) {
                let { locations, destination } = movement;
                let movedDistance = 0;
                for(let i = 1; i < locations.length; i++) {
                    let { latitude: lat1, longitude: lon1} = locations[i];
                    let { latitude: lat2, longitude: lon2} = locations[i - 1];

                    movedDistance += distanceTo(this.toLatLon(lat1, lon1), this.toLatLon(lat2, lon2));
                }

                log.debug(`Moved distance: ${movedDistance} meters`);

                // Check stopped
                if(movedDistance < 100) {
                    return this.reportStatus(driver_id, StatusTag.Stopped);
                }

                let { latitude: lat1, longitude: lon1} = _.first(locations);
                let { latitude: lat2, longitude: lon2 } = _.last(locations);

                let { latitude: lat3, longitude: lon3 } = destination;
                let distance1 = distanceTo(this.toLatLon(lat1, lon1), this.toLatLon(lat3, lon3));
                let distance2 = distanceTo(this.toLatLon(lat2, lon2), this.toLatLon(lat3, lon3));

                log.debug(`a) distance to destination ${distance1} meters`);
                log.debug(`b) distance to destination ${distance2} meters`);

                // Wrong Direction
                if(distance2 - distance1 > 1000) { // 1 km
                    return this.reportStatus(driver_id, StatusTag.WrongWay);
                }
            }

            return this.reportStatus(driver_id, StatusTag.Normal);
        });
    }

    /**
     * @param {number} driver_id
     * @returns {boolean}
     **/
    isMonitoring(driver_id) {
        return this.driversOnTrip.indexOf(driver_id) >= 0;
    }

    /**
     * @param {number} driver_id 
     * @returns {Promise<boolean>}
     */
    async startMonitoring(driver_id) {
        if (this.isMonitoring(driver_id)) return false;

        let trip = await Models.Trip.findOne({ 
            where: { driver_id },
            order: [
				['requested_at', 'DESC']
			]
        });

        if(!trip) {
            log.warn(`Cannot find trip of driver #${driver_id}`);
            return false;
        }

        this.driversOnTrip.push(driver_id);
        this.statuses.push({
            driver_id,
            tag: StatusTag.Normal,
            updated_at: moment.now()
        });
        this.connections.push({
            driver_id
        });
        this.movements.push({
            driver_id,
            checkable: false,
            departure: {
                latitude: Number(trip['src_latitude']),
                longitude: Number(trip['src_longitude'])
            },
            destination: {
                latitude: Number(trip['dst_latitude']),
                longitude: Number(trip['dst_longitude'])
            },
            locations: []
        });

        log.debug(`Start monitoring Driver #${driver_id}`);
        return true;
    }

    /**
     * @param {number} driver_id 
     * @returns {boolean}
     */
    stopMonitoring(driver_id) {
        if (!this.isMonitoring(driver_id)) return false;
        _.remove(this.driversOnTrip, d => d === driver_id);
        _.remove(this.statuses, { driver_id });
        _.remove(this.connections, { driver_id });
        _.remove(this.movements, { driver_id });

        log.debug(`Stop monitoring Driver #${driver_id}`);
        return true;
    }

    async subscribeDB() {
        let drivers = await Models.Driver.findAll({
            attributes: ['driver_id', 'user_name'],
            where: {
                status: 'on_trip'
            },
            raw: true
        });

        _.forEach(drivers, driver => this.startMonitoring(driver['driver_id']));
        
        dbService.subscriptions.driver.subscribe((driver) => {
            let {driver_id, status} = driver;

            if(status === 'on_trip') {
                this.startMonitoring(driver_id);
            } else {
                this.stopMonitoring(driver_id);
            }
        });

        dbService.subscriptions.location.subscribe(({driver_id, latitude, longitude}) => {
            this.updateLocation(driver_id, latitude, longitude);
        });
    }

    /** 
     * @param {number} driver_id 
     * @param {number} latitude
     * @param {number} longitude
     **/
    updateLocation(driver_id, latitude, longitude) {
        if(!this.isMonitoring(driver_id)) return;

        let now = moment.now();
        let movement = this.getMovement(driver_id);

        if(movement) {
            let first = _.first(movement.locations);

            movement.checkable = first && (now - first.timestamp) > KEEP_LOCATIONS_DURATION;

            _.remove(movement.locations, ({timestamp}) => {
                return (now - timestamp) > KEEP_LOCATIONS_DURATION;
            });

            movement.locations.push({
                latitude,
                longitude,
                timestamp: now
            });
        }
    }

    /** 
     * This method is invoked when driver push sos button.
     * @param {number} driver_id 
     **/
    async sos(driver_id) {
        this.reportStatus(driver_id, StatusTag.Emergency);
    }

    /** 
     * Once driver open the app, this method will be invoked. It means that driver is not in emergency.
     * @param {number} driver_id 
     **/
    async active(driver_id) {
        let status = this.getStatus(driver_id);

        if(status && status.tag === StatusTag.Emergency) {
            log.info(`Driver #${driver_id} is active now`);
            this.reportStatus(driver_id, StatusTag.Normal);
        }
    }

    /** 
     * This method should be invoked every minute while driver is on trip
     * @param {number} driver_id 
     **/
    async ping(driver_id) {
        let connection = this.getConnection(driver_id);
        if(connection) {
            connection.last_ping_at = moment.now();
        }

        let status = this.getStatus(driver_id);
        if(status && status.tag === StatusTag.ConnectionLost) {
            this.reportStatus(driver_id, StatusTag.Normal);
        }
    }
    
    get statusObservable() {
        return this.statusSubject.asObservable();
    }
    
    async start() {
        await this.subscribeDB();

        setInterval(() => {
            this.checkStatus();
        }, CHECK_STATUS_INTERVAL);        
        log.info(`Service started`);
    }
}

module.exports = new MonitorService();
