const debug = require('debug');
const moment = require('moment');

const Message = require('../models').Message;

if(process.env.NODE_ENV === 'development') {
  // override default debug.formatArgs() implementation
  debug.formatArgs = function (args) { // requires access to "this"
      const name = this.namespace;
      // example: prepend something to arguments[0]
      args[0] = `${moment().format('YYYY-MM-DD HH:mm:ss')} [${name}] ${args[0]}`;
  };
}

class Logger {
  /** @param {string} tag */
  constructor(tag) {
    this.debug = debug(`${tag}:debug`);
    this.info = debug(`${tag}:info`);
    this.warn = debug(`${tag}:warn`)
    this.error = debug(`${tag}:error`);
  }

  db({ type, publisher_name, content, meta_data, category}) {
    try {
      if(!meta_data) meta_data = {};
      Message.create({
        type,
        publisher_name,
        category,
        content,
        meta_data
      });  
    } catch (err) { this.error(`Failed to create log`, err)}    
  }
}

module.exports = /** @return {Logger} */ (/** @type {string} */tag) => {
  return new Logger(tag);
};
