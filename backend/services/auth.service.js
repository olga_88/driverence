const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const secret = process.env.JWT_SECRET || 'secret';

const authService = () => {
	const issue = (payload) => jwt.sign(payload, secret, { expiresIn: payload.expiresIn || '30d' });
  	const verify = (token, cb) => jwt.verify(token, secret, {}, cb);
	const decode = (token) => jwt.verify(token, secret);
	
	return {
		issue,
		verify,
		decode
	}
};

module.exports = authService();